package com.itheima.recommend.listener;

import com.itheima.domain.mongo.MovementScore;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class MovementScoreListener {

    @Autowired
    private MongoTemplate mongoTemplate;

    @RabbitListener(queuesToDeclare = @Queue("tanhua.recommend.movement"))
    public void listenMovementScore(MovementScore movementScore) {
        System.out.println("监听推荐日志：" + movementScore);
        movementScore.setDate(System.currentTimeMillis()); // 保存时间

        // 保存动态推荐日志
        mongoTemplate.save(movementScore);

    }
}
