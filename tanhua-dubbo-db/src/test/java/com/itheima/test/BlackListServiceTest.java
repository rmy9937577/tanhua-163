package com.itheima.test;

import com.itheima.service.db.BlackListService;
import com.itheima.vo.PageBeanVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlackListServiceTest {

    @Autowired
    private BlackListService blackListService;

    @Test
    public void test01()throws Exception{
        PageBeanVo pageBeanVo = blackListService.findByPage(110L, 1, 5);
        System.out.println(pageBeanVo);
    }
}
