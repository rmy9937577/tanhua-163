package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.AnalysisByDay;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AnalysisByDayMapper extends BaseMapper<AnalysisByDay> {

    // 累计用户
    @Select("SELECT SUM(num_registered) FROM `tb_analysis_by_day`")
    Long findCumulativeUsers();

    // 过去7、30天活跃
    @Select("SELECT SUM(num_active) FROM `tb_analysis_by_day` WHERE record_Date BETWEEN #{startTime} AND #{endTime}")
    Long  findActivePass(@Param("startTime") String startTime,@Param("endTime") String endTime);

    // 今日、昨天基础数据
    @Select("SELECT * FROM `tb_analysis_by_day` WHERE record_date = #{recordTime}")
    AnalysisByDay findBase(String recordTime);
}