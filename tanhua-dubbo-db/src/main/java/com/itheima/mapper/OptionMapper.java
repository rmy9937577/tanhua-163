package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OptionMapper extends BaseMapper<Options> {

    @Select("select * from tb_options where questions_id = #{questionsId}")
    List<Options> findByQuestionsId(Long questionsId);
    @Select("select * from tb_options where id = #{optionId}")
    Options findByOptionsId(Long optionId);
}
