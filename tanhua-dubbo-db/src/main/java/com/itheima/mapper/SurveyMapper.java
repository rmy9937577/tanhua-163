package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Survey;
public interface SurveyMapper extends BaseMapper<Survey> {
}
