package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.QuestionUserLock;

/**
 * @Author RMY
 * @Date 2021/11/13 9:49
 * @Version 1.0
 */
public interface QuestionUserLockMapper extends BaseMapper<QuestionUserLock> {
}
