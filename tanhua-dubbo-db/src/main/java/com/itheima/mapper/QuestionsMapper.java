package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Questions;

public interface QuestionsMapper extends BaseMapper<Questions> {
}
