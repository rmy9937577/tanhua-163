package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Reported;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/11 21:48
 * @Version 1.0
 */
public interface ReportedMapper extends BaseMapper<Reported> {

    //查找相差两分的用户
    @Select("SELECT DISTINCT user_id FROM tb_reported WHERE result BETWEEN #{result}-2 AND #{result}+2  AND survey_id = #{surveyId} ")
    public List<Long> findSimilarYou(Long surveyId, Integer result);

    //根据用户Id查询最新报告
    @Select("SELECT * FROM tb_reported WHERE user_id = #{userId} ORDER BY created DESC LIMIT 0,1")
    public Reported findReportedByUserId(Long userId);
}
