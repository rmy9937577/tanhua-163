package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Log;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface LogMapper extends BaseMapper<Log> {


    // 新增、登录
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE TYPE=#{type} AND log_time=#{logTime}")
    Integer findByTypeAndTime(@Param("type") String type,@Param("logTime") String logTime);

    // 活跃用户数
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE log_time=#{logTime}")
    Integer findByTime(String logTime);

    // 次日留存
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE log_time=#{today}\n" +
            " AND user_id IN (SELECT DISTINCT user_id FROM tb_log WHERE TYPE='0102' AND log_time=#{yesterday})")
    Integer findNumRetention1d(@Param("today") String  today,@Param("yesterday") String yesterday);
}