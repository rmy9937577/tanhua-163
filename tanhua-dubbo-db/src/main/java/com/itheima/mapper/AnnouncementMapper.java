package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Announcement;

/**
 * @Author RMY
 * @Date 2021/11/10 19:26
 * @Version 1.0
 */
public interface AnnouncementMapper extends BaseMapper<Announcement> {
}
