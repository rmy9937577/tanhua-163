package com.itheima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.itheima.mapper")
// 启动排除mongo自动装配
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
@EnableScheduling // 开启定时任务注解
public class DbServiceApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(DbServiceApplication.class, args);
    }
}
