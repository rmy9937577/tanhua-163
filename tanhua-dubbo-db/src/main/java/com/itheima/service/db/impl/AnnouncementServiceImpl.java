package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Announcement;
import com.itheima.mapper.AnnouncementMapper;
import com.itheima.service.db.AnnouncementService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author RMY
 * @Date 2021/11/10 19:25
 * @Version 1.0
 */
@DubboService
public class AnnouncementServiceImpl implements AnnouncementService {
    @Autowired
    private AnnouncementMapper announcementMapper;
    //查询所有公告
    @Override
    public PageBeanVo findAllAnnouncement(Integer pageNum, Integer pageSize) {
        //开启分页
        Page<Announcement> page = new Page<>(pageNum,pageSize);
        page = announcementMapper.selectPage(page, null);
        //初始化分页对象,并封装
        PageBeanVo paginationVo = new PageBeanVo();
        paginationVo.setItems(page.getRecords());
        paginationVo.setPage(pageNum);
        paginationVo.setPagesize(pageSize);
        paginationVo.setPages(page.getPages());
        paginationVo.setCounts(page.getTotal());
        //返回分页对象
        return paginationVo;
    }
}
