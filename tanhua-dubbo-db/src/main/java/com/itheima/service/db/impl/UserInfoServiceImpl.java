package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.UserInfo;
import com.itheima.mapper.UserInfoMapper;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    // 保存用户信息
    @Override
    public void save(UserInfo userInfo) {
        userInfoMapper.insert(userInfo);
    }

    @Override
    public void update(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);
    }

    // 根据id查询用户信息
    @Override
    public UserInfo findUserInfoById(Long id) {
        return userInfoMapper.selectById(id);
    }

    // 分页查询
    @Override
    public PageBeanVo findUserInfoByPage(Integer pageNum, Integer pageSize) {
        // 1.设置分页
        Page<UserInfo> page = new Page<>(pageNum, pageSize);

        // 2.分页查询
        page = userInfoMapper.selectPage(page, null);

        // 3.封装pbv并返回
        return new PageBeanVo(pageNum, pageSize, page.getTotal(), page.getRecords());
    }
}
