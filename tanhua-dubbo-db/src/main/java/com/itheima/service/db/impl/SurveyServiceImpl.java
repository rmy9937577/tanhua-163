package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Questions;
import com.itheima.domain.db.Survey;
import com.itheima.mapper.SurveyMapper;
import com.itheima.service.db.OptionService;
import com.itheima.service.db.QuestionsService;
import com.itheima.service.db.SurveyService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;

@DubboService
public class SurveyServiceImpl implements SurveyService {
    @Autowired
    private SurveyMapper surveyMapper;
    //分页查找问卷
    @Override
    public Page<Survey> findSurveyByPage(Integer pageNum, Integer pageSize) {
        //开启分页
        Page<Survey> page=new Page(pageNum,pageSize);
        //查询分页
         page= surveyMapper.selectPage(page,null);
         return page;
    }
    //更新问卷
    @Override
    public void update(Survey survey) {
        surveyMapper.updateById(survey);
    }
    //通过id查找
    @Override
    public Survey findSurveyById(Long id) {
        return surveyMapper.selectById(id);
    }

    //保存问卷
    @Override
    public void saveSurvey(Survey survey) {
        surveyMapper.insert(survey);
    }

    @Autowired
    private QuestionsService questionsService;
    @Autowired
    private OptionService optionService;
    //根据id删除问卷
    @Override
    public void deleteById(Long id) {
       //先找题目
        List<Questions> questionList=questionsService.findQuestionBySurveyId(id.longValue());
        for (Questions questions : questionList) {
            //先删选项
            optionService.deleteOptionByQuestionId(questions.getId());
            //删题目
            questionsService.deleteById(id);
        }
        QueryWrapper qw=new QueryWrapper();
        qw.eq("id",id);
        surveyMapper.delete(qw);
    }


}
