package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Options;
import com.itheima.mapper.OptionMapper;
import com.itheima.service.db.OptionService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class OptionServiceImpl implements OptionService {
    @Autowired
    private OptionMapper optionMapper;
    //保存题目
    @Override
    public void saveOption(Options options) {
        optionMapper.insert(options);
    }
    //删除

//分页查询
    @Override
    public Page<Options> findByPage(Long questionId, Integer pageNum, Integer pageSize) {
        //开启分页
        Page page = new Page(pageNum, pageSize);
        QueryWrapper<Options> wq = new QueryWrapper<>();
        wq.eq("questions_id",questionId);
        page = optionMapper.selectPage(page, wq);
        return page;
    }

    //通过id删除
    @Override
    public void deleteOption(Long id) {
        optionMapper.deleteById(id);
    }
    //通过题目id删除选项
    @Override
    public void deleteOptionByQuestionId(Long id) {
        QueryWrapper<Options> qw = new QueryWrapper<>();
        qw.eq("questions_id",id);
        optionMapper.delete(qw);
    }
    //通过id查询
    @Override
    public Options findOptionsById(Long id) {
        return optionMapper.selectById(id);

    }
    //更新选项
    @Override
    public void updateOptions(Options options) {
        optionMapper.updateById(options);
    }
}
