package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Admin;
import com.itheima.mapper.AdminMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.AdminService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin findByUsername(String username) {
        // 1.创建条件对象
        QueryWrapper<Admin> qw = new QueryWrapper<>();
        qw.eq("username", username);
        // 2.调用mapper查询
        return adminMapper.selectOne(qw);
    }
}
