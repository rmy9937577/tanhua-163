package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Notification;
import com.itheima.mapper.NotificationMapper;
import com.itheima.service.db.NotificationService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationMapper notificationMapper;

    @Override
    public Notification findByUserId(Long userId) {
        // 创建查询对象
        QueryWrapper<Notification> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        return notificationMapper.selectOne(qw);
    }

    // 保存推送通知
    @Override
    public void save(Notification notification) {
        notificationMapper.insert(notification);
    }

    // 更新推送通知
    @Override
    public void update(Notification notification) {
        notificationMapper.updateById(notification);
    }
}
