package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.BlackList;
import com.itheima.mapper.BlackListMapper;
import com.itheima.service.db.BlackListService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class BlackListServiceImpl implements BlackListService {

    @Autowired
    private BlackListMapper blackListMapper;

    @Override
    public PageBeanVo findByPage(Long userId, Integer pageNum, Integer pageSize) {
        // 1.开启分页
        Page<BlackList> page = new Page<>(pageNum, pageSize);
        // 2.分页查询
        QueryWrapper<BlackList> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        page = blackListMapper.selectPage(page, qw);
        // 3.封装pageBeanVo
        return new PageBeanVo(pageNum, pageSize, page.getTotal(), page.getRecords());
    }

    // 移除黑名单
    @Override
    public void deleteByCondition(Long userId, Long blackUserId) {
        // 1.构建条件
        QueryWrapper<BlackList> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        qw.eq("black_user_id", blackUserId);
        // 2.删除
        blackListMapper.delete(qw);
    }
}
