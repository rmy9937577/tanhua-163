package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.*;
import com.itheima.mapper.*;
import com.itheima.service.db.TestSoulService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/11 21:53
 * @Version 1.0
 */
@DubboService
public class TestSoulServiceImpl implements TestSoulService {
    @Autowired
    private SurveyMapper surveyMapper;
    @Autowired
    private QuestionsMapper questionsMapper;
    @Autowired
    private OptionMapper optionMapper;
    @Autowired
    private ReportedMapper reportedMapper;
    @Autowired
    private QuestionUserLockMapper questionUserLockMapper;
    //保存一套初级试题
    @Override
    public void saveSurvey(Survey survey) {
        surveyMapper.insert(survey);
    }

    //重新测试(更新)一套试题
    @Override
    public void updateReported(Reported reported) {
        reportedMapper.updateById(reported);
    }

    //查询一套试题
    @Override
    public List<Survey> findSurveyBySurveyIdList(List<Long> surveyIdList) {
        //查询Survey，创建查询条件
        QueryWrapper<Survey> qw = new QueryWrapper<>();
        qw.in("id",surveyIdList);
        return surveyMapper.selectList(qw);
    }

    //根据问卷id的集合查出所有问题
    @Override
    public List<Questions> findQuestionsBySurveyId(Long surveyId){
        //创建查询对象
        QueryWrapper<Questions> qw = new QueryWrapper<>();
        qw.eq("survey_id",surveyId);
        //查询结果
        List<Questions> questionsList = questionsMapper.selectList(qw);
        //返回结果
        return questionsList;
    }
    //根据问题id查询该问题的所有选项
    @Override
    public List<Options> findOptionsByQuestionsId(Long questionsId) {
        //初始化条件查询
        QueryWrapper<Options> qw = new QueryWrapper<>();
        //确定查询条件
        qw.eq("questions_id",questionsId);
        List<Options> optionsList = optionMapper.findByQuestionsId(questionsId);
        return optionsList;
    }
    //根据选项id找到对应分数
    @Override
    public Options findByOptionId(long parseLong) {
        //查询
        Options options = optionMapper.findByOptionsId(parseLong);
        return options;
    }
    //将计算的结果保存reported表
    @Override
    public Long saveReported(Reported reported) {
        reportedMapper.insert(reported);
        return reported.getId();
    }
    //根据结果Id查找结果
    @Override
    public Reported findByReportedId(Long reportedId) {
        return reportedMapper.selectById(reportedId);
    }
    //根据id查找问题
    @Override
    public Questions findQuestionsByQuestionsId(Long questionsId) {
        return questionsMapper.selectById(questionsId);
    }
    //查找分数相似度较高的用户
    @Override
    public List<Long> findSimilarYou(Long surveyId,Integer result) {
        //调用mapper
        return reportedMapper.findSimilarYou(surveyId,result);
    }


    //查询用户锁题表
    @Override
    public List<QuestionUserLock> findQuestionUserLockByUserId(Long userId) {
        //创建查询对象
        QueryWrapper<QuestionUserLock> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        return questionUserLockMapper.selectList(qw);
    }
    //根据问卷id查询锁表
    @Override
    public QuestionUserLock findQuestionUserLockByUserIdAndSurveyId(Long userId, Long surveyId) {
        QueryWrapper<QuestionUserLock> qw = new QueryWrapper<>();
        //条件id+1
        qw.eq("survey_id",surveyId+1);
        qw.eq("user_id",userId);
        return questionUserLockMapper.selectOne(qw);
    }
    //保存一个锁表记录
    @Override
    public void saveQuestionUserLock(QuestionUserLock questionUserLock) {
        questionUserLockMapper.insert(questionUserLock);
    }
    //更新锁表
    @Override
    public void updateQuestionUserLock(QuestionUserLock questionUserLock) {
        questionUserLockMapper.updateById(questionUserLock);
    }
    //根据用户Id查询他的所有成绩记录
    @Override
    public Reported findReportedByUserId(Long userId) {
        return reportedMapper.findReportedByUserId(userId);
    }
}
