package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.Questions;
import com.itheima.mapper.OptionMapper;
import com.itheima.mapper.QuestionsMapper;
import com.itheima.service.db.QuestionsService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@DubboService
public class QuestionsServiceImpl implements QuestionsService {
    @Autowired
    private QuestionsMapper questionMapper;
    @Autowired
    private OptionMapper optionMapper;
    //保存题目
    @Override
    public void saveQuestion(Questions questions) {
        questionMapper.insert(questions);
    }
    //通过id删除题目
    @Override
    public void deleteById(Long id) {
        QueryWrapper qw=new QueryWrapper();
        qw.eq("questions_id",id);
        //先删除选项
        optionMapper.delete(qw);
        //删除题目
        questionMapper.deleteById(id);
    }
    //通过问卷id分页查找问题
    @Override
    public Page<Questions> findQuestionByPage(Long surveyId,Integer pageNum, Integer pageSize) {
        Page page=new Page(pageNum,pageSize);
        //查询分页
        QueryWrapper<Questions> qw = new QueryWrapper<>();
        qw.eq("survey_id",surveyId);
        page=questionMapper.selectPage(page,qw);
        return page;
    }

    //通过问卷id查问题
    @Override
    public List<Questions> findQuestionBySurveyId(Long id) {
        QueryWrapper<Questions> qw = new QueryWrapper<>();
        qw.eq("survey_id",id);
        return questionMapper.selectList(qw);
    }
    //通过id查找
    @Override
    public Questions findQuestionById(Long id) {
        return questionMapper.selectById(id);
    }
    //更新问题
    @Override
    public void update(Questions questions) {
        questionMapper.updateById(questions);
    }
}
