package com.itheima.service.db.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.itheima.domain.db.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@DubboService
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Long save(User user) {
        // 对密码进行加密
        String md5Pwd = SecureUtil.md5(user.getPassword());
        user.setPassword(md5Pwd);
        /*// 指定时间
        user.setCreated(new Date());
        user.setUpdated(new Date());*/
        userMapper.insert(user);
        return user.getId();
    }

    @Override
    public User findByPhone(String phone) {
        // 创建查询对象
        QueryWrapper<User> qw = new QueryWrapper<>();
        // 指定条件
        qw.eq("phone", phone);
        // 执行查询
        return userMapper.selectOne(qw);
    }
    @Override
    public void update(Long id,String phone) {
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", id).set("phone" ,phone );
        userMapper.update(null, wrapper);
    }

    // 根据id查询用户信息
    @Override
    public User findById(Long userId) {
        return userMapper.selectById(userId);
    }
}
