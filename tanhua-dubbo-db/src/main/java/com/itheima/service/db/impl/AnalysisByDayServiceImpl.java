package com.itheima.service.db.impl;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.AnalysisByDay;
import com.itheima.mapper.AnalysisByDayMapper;
import com.itheima.mapper.LogMapper;
import com.itheima.service.db.AnalysisByDayService;
import com.itheima.util.ComputeUtil;
import com.itheima.vo.AnalysisSummaryVo;
import com.itheima.vo.DashboardVo;
import com.itheima.vo.LineChartVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DubboService
public class AnalysisByDayServiceImpl implements AnalysisByDayService {

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private AnalysisByDayMapper analysisByDayMapper;

    @Override
    public void logToAnalysisByDay() {
        // 1.准备基础数据
        // 1.1今天
        String today = DateUtil.offsetDay(new Date(), 0).toDateStr();
        // 1.2昨天
        String yesterday = DateUtil.offsetDay(new Date(), -1).toDateStr();
        // 2.查询log数据
        // 2.1注册
        Integer numRegistered = logMapper.findByTypeAndTime("0102", today);
        // 2.2活跃
        Integer numActive = logMapper.findByTime(today);
        // 2.3登录
        Integer numLogin = logMapper.findByTypeAndTime("0101", today);
        // 2.4次日
        Integer numRetention1d = logMapper.findNumRetention1d(today, yesterday);

        // 3.查询今天是否有数据
        QueryWrapper<AnalysisByDay> qw = new QueryWrapper<>();
        qw.eq("record_date", today);
        AnalysisByDay analysisByDay = analysisByDayMapper.selectOne(qw);

        // 4.判断
        if (analysisByDay == null) { // 新增
            analysisByDay = new AnalysisByDay();
            analysisByDay.setRecordDate(new Date());
            analysisByDay.setNumRegistered(numRegistered);
            analysisByDay.setNumActive(numActive);
            analysisByDay.setNumLogin(numLogin);
            analysisByDay.setNumRetention1d(numRetention1d);
            analysisByDayMapper.insert(analysisByDay);  // 注意这里是insert语句
        } else { // 更新
            analysisByDay.setNumRegistered(numRegistered);
            analysisByDay.setNumActive(numActive);
            analysisByDay.setNumLogin(numLogin);
            analysisByDay.setNumRetention1d(numRetention1d);
            analysisByDayMapper.updateById(analysisByDay); // 注意这里是update语句
        }

    }

    // 首页统计展示
    @Override
    public AnalysisSummaryVo findAnalysisSummaryVo() {
        // 1.准备基础数据
        // 1.1 今天
        String today = DateUtil.offsetDay(new Date(), 0).toDateStr();
        // 1.2 昨天
        String yesterday = DateUtil.offsetDay(new Date(), -1).toDateStr();
        // 1.3 前七天
        String weekday = DateUtil.offsetDay(new Date(), -7).toDateStr();
        // 1.4 前30天
        String monthday = DateUtil.offsetDay(new Date(), -30).toDateStr();
        // 1.5 今日内容
        AnalysisByDay todayAna = analysisByDayMapper.findBase(today);
        // 1.6 昨天内容
        AnalysisByDay yesterdayAna = analysisByDayMapper.findBase(yesterday);

        // 2.凑齐9份数据
        // 2.1 累计用户数
        Long cumulativeUsers = analysisByDayMapper.findCumulativeUsers();
        // 2.2 过去30天活跃用户数
        Long activePassMonth = analysisByDayMapper.findActivePass(monthday, today);
        // 2.3 过去7天活跃用户
        Long activePassWeek = analysisByDayMapper.findActivePass(weekday, today);
        // 2.4 今日新增用户数量
        Integer newUsersToday = todayAna.getNumRegistered();
        // 2.5 今日新增用户涨跌率，单位百分数，正数为涨，负数为跌   BigDecimal : 商业数字格式
        Integer yesterdayReg = yesterdayAna.getNumRegistered();
        BigDecimal newUsersTodayRate = ComputeUtil.computeRate(newUsersToday, yesterdayReg);
        // 2.6 今日登录次数
        Integer loginTimesToday = todayAna.getNumLogin();
        // 2.7 今日登录次数涨跌率，单位百分数，正数为涨，负数为跌
        Integer yesterdayLogin = yesterdayAna.getNumLogin();
        BigDecimal loginTimesTodayRate = ComputeUtil.computeRate(loginTimesToday, yesterdayLogin);
        // 2.8 今日活跃用户数量
        Integer activeUsersToday = todayAna.getNumActive();
        // 2.9 今日活跃用户涨跌率，单位百分数，正数为涨，负数为跌
        Integer yesterdayActive = yesterdayAna.getNumActive();
        BigDecimal activeUsersTodayRate = ComputeUtil.computeRate(activeUsersToday, yesterdayActive);

        // 3.封装并返回vo
        AnalysisSummaryVo vo = new AnalysisSummaryVo();
        vo.setCumulativeUsers(cumulativeUsers);
        vo.setActivePassMonth(activePassMonth);
        vo.setActivePassWeek(activePassWeek);
        vo.setNewUsersToday(newUsersToday.longValue());
        vo.setNewUsersTodayRate(newUsersTodayRate);
        vo.setLoginTimesToday(loginTimesToday.longValue());
        vo.setLoginTimesTodayRate(loginTimesTodayRate);
        vo.setActiveUsersToday(activeUsersToday.longValue());
        vo.setActiveUsersTodayRate(activeUsersTodayRate);

        return vo;
    }


    // 主页展示新增、活跃用户、次日留存率折线图
    @Override
    public List<DashboardVo> findDashboardVo(Long startTime, Long endTime, String type) {
        // 算出最后一天
        String endDay = DateUtil.offsetDay(new Date(endTime), 0).toDateStr();
        // 声明一个集合
        List<DashboardVo> dashboardVos = new ArrayList<>();
        // 声明一个常量
        Integer day = 0;
        // 进行循环操作
        while (!endDay.equals(DateUtil.offsetDay(new Date(startTime), day).toDateStr())) {
            // 声明对象
            DashboardVo dashboardVo = new DashboardVo();
            // 算出当前天数
            String startDay = DateUtil.offsetDay(new Date(startTime), day).toDateStr();
            day++;
            // 创建条件
            QueryWrapper<AnalysisByDay> qw = new QueryWrapper<>();
            qw.eq("record_date", startDay);
            qw.orderByDesc("created");
            // 查询数据库
            List<AnalysisByDay> analysisByDays = analysisByDayMapper.selectList(qw);
            // 判空并去第一个
            if (CollectionUtil.isNotEmpty(analysisByDays)) {
                AnalysisByDay analysisByDay = analysisByDays.get(0);
                // type选择
                switch (type) {
                    case "101": // 注册
                        dashboardVo.setAmount(analysisByDay.getNumRegistered());
                        break;
                    case "102":  // 活跃
                        dashboardVo.setAmount(analysisByDay.getNumActive());
                        break;
                    case "103":  // 次日登录
                        dashboardVo.setAmount(analysisByDay.getNumRetention1d());
                        break;
                }
            } else { // 没有数据都为0
                dashboardVo.setAmount(0);
            }
            // 封装时间
            dashboardVo.setTitle(startDay);
            // 添加到集合中
            dashboardVos.add(dashboardVo);
        }
        return dashboardVos;
    }



}
