package com.itheima.job;

import com.itheima.service.db.AnalysisByDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class AnalysisJob {

    @Autowired
    private AnalysisByDayService analysisByDayService;

    @Scheduled(cron = "0 0 0/1 * * ?") // 每间隔1小时
    // @Scheduled(cron = "0/30 * * * * ?") // 每间隔30秒
    public void logAnalysisJob(){
        System.out.println("日志统计开始...");
        analysisByDayService.logToAnalysisByDay();
        System.out.println("日志统计结束...");

    }
}
