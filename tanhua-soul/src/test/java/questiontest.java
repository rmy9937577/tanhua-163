import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.SoulApplication;
import com.itheima.domain.db.Questions;
import com.itheima.service.db.QuestionsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SoulApplication.class)
public class questiontest {
    @Autowired
    private QuestionsService questionsService;
    @Test
    public void test() throws Exception{
        Page<Questions> page = questionsService.findQuestionByPage(2l, 1, 10);
        List<Questions> records = page.getRecords();
        for (Questions record : records) {
            System.out.println(record);
        }
    }
    @Test
    public void test2() throws Exception{
        Questions questions=new Questions();
        questions.setId(1l);
        questionsService.deleteById(1l);

    }
}
