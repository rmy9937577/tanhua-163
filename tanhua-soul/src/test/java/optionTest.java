import com.itheima.SoulApplication;
import com.itheima.domain.db.Options;
import com.itheima.service.db.OptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SoulApplication.class)
public class optionTest {
    @Autowired
    private OptionService optionService;
    @Test
    public void test() throws Exception{
        Options options =new Options();
        options.setQuestionsId(1l);
        options.setOpt("选项1");
        options.setScore(2);
        optionService.saveOption(options);
    }
}
