package com.itheima.utils;



import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;



public class BaiduMapUtils {
  //获取地址
    /**
     *
     * @param ky 百度地图 key
     * @param location 经纬度
     * @return address
     */
    public static String getAddress(String ky,String location){
        //请求地址
        String url="https://api.map.baidu.com/reverse_geocoding/v3/?ak="+ky+"&output=json&coordtype=wgs84ll&location="+location;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        String body = forEntity.getBody();
        JSONObject jsonObject = JSON.parseObject(body);
        JSONObject result = (JSONObject) jsonObject.get("result");
        Object formatted_address = result.get("formatted_address");
        return formatted_address.toString();
    }

}
