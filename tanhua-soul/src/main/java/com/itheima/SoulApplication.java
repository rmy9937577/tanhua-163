package com.itheima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@MapperScan("com.itheima.mapper")
// 启动排除mongo自动装配
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
public class SoulApplication {
    public static void main(String[] args) {
        SpringApplication.run(SoulApplication.class,args);
    }
}
