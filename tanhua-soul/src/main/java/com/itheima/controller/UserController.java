package com.itheima.controller;

import com.itheima.manager.UserManager;
import com.itheima.vo.UserAndMapVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class UserController {
    @Autowired
    private UserManager userManager;

    // 分页查询
    @GetMapping("/soul/users")
    public ResponseEntity findUserInfoByPage(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        // 调用manager
        return userManager.findUserInfoByPage(pageNum, pageSize);
    }
    @GetMapping("soul/getUserById")
    public ResponseEntity findById(Long id){
        return ResponseEntity.ok(userManager.findUserById(id));
    }
}
