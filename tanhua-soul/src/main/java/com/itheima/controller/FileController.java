package com.itheima.controller;

import com.itheima.autoconfig.oss.OssTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
@CrossOrigin("*")
@RestController
public class FileController {
    @Autowired
    private OssTemplate ossTemplate;
    @PostMapping("/file/upload")
    @ResponseBody
    public String upload(MultipartFile uploadFile) throws IOException {
        String picUrl = ossTemplate.upload(uploadFile.getOriginalFilename(), uploadFile.getInputStream());
        return picUrl;
    }
}
