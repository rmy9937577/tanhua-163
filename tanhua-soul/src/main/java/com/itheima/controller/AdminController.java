package com.itheima.controller;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import com.itheima.domain.db.Admin;
import com.itheima.domain.db.User;
import com.itheima.manager.UserManager;
import com.itheima.service.db.AdminService;
import com.itheima.util.JwtUtil;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
@CrossOrigin("*")
@RestController
public class AdminController {
    @Autowired
    private UserManager userManager;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @GetMapping("/sendSms")
    public String sendSms(String telephone){
        //生成6位随机验证码
        String smsCode = RandomUtil.randomNumbers(6);
        smsCode="123456";
        //向session中存数据//5分钟有效
        stringRedisTemplate.opsForValue().set("sms_"+telephone,smsCode,5, TimeUnit.MINUTES);
        System.err.println(smsCode);
        return "ok";
    }
    @DubboReference
    private AdminService adminService;
    @PostMapping("/login")
    public Map<String,Object> login(@RequestBody Map<String,String> params, HttpSession session){
        //声明返回集合
        Map<String,Object> loginResult=new HashMap<>();
        // 1.接收参数
        String telephone=params.get("telephone");
        String smsCode = params.get("smsCode");
        // 2.从session中获取验证码
        //String codeFormSession = (String) session.getAttribute("sms_" + telephone);
        String codeFormSession =  stringRedisTemplate.opsForValue().get("sms_" + telephone);
        // 3.判断验证码
        if (smsCode==null||codeFormSession==null||(!smsCode.equals(codeFormSession))){
            loginResult.put("code",0);
            return loginResult;
        }


        // 清除验证码
        //session.removeAttribute("sms_"+telephone);
        stringRedisTemplate.delete("sms_" + telephone);
        // 4.根据手机号查询
        User user=new User();
        user.setPhone(telephone);
        if (user==null){
            loginResult.put("code",0);
            return loginResult;
        }

        // 5.登录成功了...
        user.setPassword(null);
        //user转map
        Map<String, Object> claims = BeanUtil.beanToMap(user);

        //通过jwt制作令牌
        String token = JwtUtil.createToken(claims);
        loginResult.put("code",1);
        loginResult.put("token",token);
        return loginResult;
//        session.setAttribute("user",user);
//        loginResult.put("code",1);
//        return loginResult;
    }
    @GetMapping("/verify")
    public String verify(@RequestHeader("Authorization") String token ){
        try {
            Map map=JwtUtil.parseToken(token);
            System.out.println(map);
        }catch (Exception e){
            e.printStackTrace();
            return "401";//权限不足
        }
        return "ok";
    }
}
