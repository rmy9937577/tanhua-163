package com.itheima.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PageBean implements Serializable {
    private Integer pageNum;//当前页码
    private Integer pagesize;//页大小
    private Long pages = 0L;//总页数
    private Long total = 0L;//总记录数
    private List<?> list = Collections.emptyList(); //列表

    public PageBean(Integer pageNum, Integer pageSize, Long total, List list) {
        this.pageNum = pageNum;
        this.pagesize = pageSize;
        this.list = list;
        this.total = total;
        this.pages = total % pagesize == 0 ? (total / pagesize) : (total / pagesize) + 1;
    }
}
