package com.itheima.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Options;
import com.itheima.domain.db.Questions;
import com.itheima.domain.db.Survey;
import com.itheima.manager.SurveyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
;

@RestController
@CrossOrigin("*")
public class SurveyController {
    @Autowired
    private SurveyManager surveyManager;
    //通过id查找问卷
    @GetMapping("survey/findById")
    public Survey findSurveyById(Long id){
        return surveyManager.findSurveyById(id);
    }
    //保存问卷
    @PostMapping("survey/saveSurvey")
    public String saveSurvey(@RequestBody Survey survey) {
        if (survey.getId()==null){
            surveyManager.saveSurvey(survey);
        }else {
            surveyManager.updateSurvey(survey);
        }
        return "ok";
    }
    //分页查询问卷
    @GetMapping("survey/findByPage")
    public ResponseEntity findSurveyByPage(@RequestParam(defaultValue = "1")Integer pageNum,@RequestParam(defaultValue = "10") Integer pageSize){
        return surveyManager.findSurveyByPage(pageNum,pageSize);
    }
    //删除问卷
    @DeleteMapping("survey/deleteById")
    public String deleteById(Long id){
       // surveyManager
        surveyManager.deleteById(id);
        return "ok";
    }
    //分页查询题目
    @GetMapping("/questions/findByPageAndId")
    public ResponseEntity findQuestionByPageAndId(
            Long id,
            @RequestParam(defaultValue = "1")Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize){
        Page<Questions> page = surveyManager.findQuestionByPageAndId(id, pageNum, pageSize);
        return ResponseEntity.ok(new PageBean(pageNum,pageSize,page.getTotal(), page.getRecords()));
    }
    //通过id查询
    @GetMapping("/questions/findById")
    public Questions findQuestionById(Long id){
        return surveyManager.findQuestionById(id);
    }
    //保存题目
    @PostMapping("questions/saveQuestion")
    public String saveQuestion(@RequestBody Questions questions){
        if (questions.getId()==null){
            surveyManager.saveQuestion(questions);
        }else {
            surveyManager.updateQuestions(questions);
        }
        return "ok";
    }
    //删除题目
    @DeleteMapping("questions/deleteById")
    public String deleteQuestionById(Long id){
        // surveyManager
        surveyManager.deleteQuestionById(id);
        return "ok";
    }
    //分页查询选项
    @GetMapping("/options/findByPageAndId")
    public ResponseEntity findOptionByPageAndId(
            Long id,
            @RequestParam(defaultValue = "1")Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize){
        Page<Options> page = surveyManager.findOptionByPageAndId(id, pageNum, pageSize);
        return ResponseEntity.ok(new PageBean(pageNum,pageSize,page.getTotal(), page.getRecords()));
    }
    //通过id查找选项
    @GetMapping("options/findById")
    public Options findOptionsById(Long id){
        return surveyManager.findOptionsById(id);
    }
    //保存(更新)选项
    @PostMapping("options/saveOption")
    public String saveOption(@RequestBody Options options){
        surveyManager.saveOption(options);
        return "ok";
    }
    //删除题目
    @DeleteMapping("options/deleteById")
    public String deleteOptionById(Long id){
        // surveyManager
        surveyManager.deleteOptionById(id);
        return "ok";
    }
}
