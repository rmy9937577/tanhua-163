package com.itheima.manager;

import com.itheima.controller.PageBean;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.UserLocation;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.UserLocationService;
import com.itheima.utils.BaiduMapUtils;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.UserAndMapVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
public class UserManager {
    @DubboReference
    private UserInfoService userInfoService;
    public ResponseEntity findUserInfoByPage(Integer pageNum, Integer pageSize) {
        // 1.调用rpc查询
        PageBeanVo pageBeanVo = userInfoService.findUserInfoByPage(pageNum, pageSize);
        // 2.返回
        return ResponseEntity.ok(new PageBean(pageNum,pageSize,pageBeanVo.getCounts(),pageBeanVo.getItems()));
    }
    @DubboReference
    private UserLocationService userLocationService;
    public ResponseEntity findUserById(Long id) {
        //从MySQL查userInfo
        UserInfo userInfo = userInfoService.findUserInfoById(id);
        //从mongo查地址
        UserAndMapVo vo = userLocationService.findByUserId(id);
        double longitude = vo.getLongitude();//经度
        double latitude = vo.getLatitude();//纬度
        String address=latitude+","+longitude;
        String ky="3jSM556yifDs7D2VcriyKewBX0by4tG7";
        //获得真实地址
        address = BaiduMapUtils.getAddress(ky, address);
        //封装vo
        vo.setId(userInfo.getId());
        vo.setNickname(userInfo.getNickname());
        vo.setAvatar(userInfo.getAvatar());
        vo.setCity(userInfo.getCity());
        vo.setGender(userInfo.getGender());
        vo.setLongitude(longitude);
        vo.setLatitude(latitude);
        vo.setAddress(address);
        return ResponseEntity.ok(vo);
    }
}
