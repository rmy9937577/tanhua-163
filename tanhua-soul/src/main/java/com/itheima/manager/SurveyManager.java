package com.itheima.manager;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.autoconfig.oss.OssTemplate;
import com.itheima.controller.PageBean;
import com.itheima.domain.db.Options;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.Questions;
import com.itheima.domain.db.Survey;
import com.itheima.service.db.OptionService;
import com.itheima.service.db.QuestionsService;
import com.itheima.service.db.SurveyService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
@Component
public class SurveyManager {
    @DubboReference
    private SurveyService surveyService;
    @DubboReference
    private QuestionsService questionsService;
    @DubboReference
    private OptionService optionService;
    //通过id查找问卷
    public Survey findSurveyById(Long id) {
        return surveyService.findSurveyById(id);
    }
    //保存问卷
    public void saveSurvey(Survey survey) {
        // 调用rpc保存
        surveyService.saveSurvey(survey);
    }
    //分页查询问卷
    public ResponseEntity findSurveyByPage(Integer pageNum, Integer pageSize) {
        //调用rpc查询分页
        Page<Survey> page = surveyService.findSurveyByPage(pageNum, pageSize);
        //封装分页
        PageBean pageBean = new PageBean(pageNum,pageSize,page.getTotal(),page.getRecords());

        return ResponseEntity.ok(pageBean);
    }
    //通过id删除问卷
    public void  deleteById(long id){
        surveyService.deleteById(id);
    }
    //更新问卷
    public void updateSurvey(Survey survey) {
        surveyService.update(survey);
    }
    //分页查找题目
    public Page<Questions> findQuestionByPageAndId(Long id, Integer pageNum, Integer pageSize) {
        Page<Questions> page = questionsService.findQuestionByPage(id, pageNum, pageSize);
        return page;
    }
    //保存题目
    public void saveQuestion(Questions questions){
        questionsService.saveQuestion(questions);
    }
    //通过id查询题目
    public Questions findQuestionById(Long id) {
        return questionsService.findQuestionById(id);
    }
    //修改题目
    public void updateQuestions(Questions questions) {
        questionsService.update(questions);
    }
    //通过id删除题目
    public void  deleteQuestionById(Long id){
        questionsService.deleteById(id);
    }
    //分页查询选项
    public Page<Options> findOptionByPageAndId(Long id, Integer pageNum, Integer pageSize) {
        Page<Options> page = optionService.findByPage(id, pageNum, pageSize);
        return page;
    }
    //通过id查询
    public Options findOptionsById(Long id) {
        return optionService.findOptionsById(id);

    }
    //保存选项
    public void saveOption(Options options) {
        if (options.getId()==null){//id为空，新增选项
            optionService.saveOption(options);
        }else {//不为空，更新
            optionService.updateOptions(options);
        }

    }
    //删除选项
    public void deleteOptionById(Long id) {
        optionService.deleteOption(id);
    }
}
