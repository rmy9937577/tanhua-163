package com.itheima.vo;

import com.itheima.domain.db.Dimensions;
import com.itheima.domain.db.SimilarYou;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/12 20:36
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportedVo implements Serializable {

    private String conclusion;//鉴定结果

    private String cover;//结果封面

    private List<Dimensions> dimensions;//维度

    private List<SimilarYou> similarYou;//与你相似

}
