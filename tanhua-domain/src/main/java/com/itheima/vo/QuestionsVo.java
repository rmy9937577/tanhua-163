package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/11 23:13
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionsVo implements Serializable {

    private String id;

    private String question;//问题

    //private String level;//题库的级别

    private List<OptionsVo> options;//选项
}
