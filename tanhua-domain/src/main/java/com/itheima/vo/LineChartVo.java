package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LineChartVo implements Serializable {

    private List<DashboardVo> thisYear; // 今年的数据

    private List<DashboardVo> lastYear; // 去年的数据

}
