package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author RMY
 * @Date 2021/11/6 14:25
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementVo implements Serializable {

    private String id;//公告id

    private String title;//公告标题

    private String description;//描述

    private  String createDate;//发布时间
}
