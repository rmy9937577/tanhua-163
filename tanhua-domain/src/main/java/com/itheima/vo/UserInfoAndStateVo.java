package com.itheima.vo;


import cn.hutool.core.bean.BeanUtil;
import com.itheima.domain.db.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

// 用户的信息和状态对象
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoAndStateVo implements Serializable {

    private Integer id;// 用户id
    private String avatar;// 用户头像地址
    private String nickname; // 昵称
    private String gender;// 性别
    private Integer age; // 年龄
    private String userStatus = "1";// 用户状态 默认为正常
    private String city;// 注册城市
    private Integer income;// 收入
    private Long created; // 注册时间
    private String tags; // 标签
    private String marriage;//是否已婚

    // 赋值
    public void setUserInfo(UserInfo userInfo) {
        this.id = userInfo.getId().intValue();
        this.avatar = userInfo.getAvatar();
        this.nickname = userInfo.getNickname();
        this.gender = userInfo.getGender();
        this.age = userInfo.getAge();
        this.city = userInfo.getCity();
        this.income = Integer.parseInt(userInfo.getIncome());
        this.created = userInfo.getCreated().getTime();
        this.tags = userInfo.getTags();
        this.marriage = userInfo.getMarriage()+"";
    }
}
