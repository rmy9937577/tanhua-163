package com.itheima.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/11 23:09
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SurveyVo implements Serializable {

    private String id;

    private String name;//问卷名 枚举：初级灵魂题,中级灵魂题,高级灵魂题

    private String cover;//封面

    private String level;//级别 枚举: 初级,中级,高级

    private Integer star; //星别（例如：2颗星，3颗星，5颗星）

    private Integer isLock;//是否锁住（0解锁，1锁住）

    private List<QuestionsVo> questions;//问题

    private String reportId;//最新报告id  //不提交问卷就没有


}
