package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author RMY
 * @Date 2021/11/12 11:50
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OptionsVo implements Serializable {

    private String id;

    private String option;//选项

}
