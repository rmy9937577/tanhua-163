package com.itheima.vo;

import com.itheima.domain.db.UserInfo;
import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SlideCardsVo {

    private Integer id; // 用户id
    private String avatar; // 头像
    private String nickname;  // 昵称
    private String gender;  // 性别
    private Integer age;     // 年龄
    private String[] tags;   // 标签

    public void setUserInfo(UserInfo userInfo) {
        this.id = userInfo.getId().intValue();
        this.avatar = userInfo.getAvatar();
        this.nickname = userInfo.getNickname();
        this.gender = userInfo.getGender();
        this.age = userInfo.getAge();
        this.tags = userInfo.getTags().split(",");
    }
}
