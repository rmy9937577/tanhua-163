package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "sound")
public class Sound implements Serializable {

    private ObjectId id; //主键id 音频的id
    private Long userId;//用户的id
    private Long random;//唯一标识用来随机的
    private String soundUrl; //语音地址
    private Long created; //创建时间

}
