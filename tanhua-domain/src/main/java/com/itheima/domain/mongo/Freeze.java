package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "freeze")
public class Freeze implements Serializable {

    private ObjectId id; //主键id
    private Integer userId;//冻结用户id
    private Integer freezingTime; //冻结时间，1为冻结3天，2为冻结7天，3为永久冻结
    private Integer freezingRange; //冻结范围，1为冻结登录，2为冻结发言，3为冻结发布动态
    private String reasonsForFreezing; // 冻结原因
    private String frozenRemarks; // 冻结备注

    private String userStatus; // 冻结状态


    private String reasonsForThawing = ""; // 解冻原因

    private Long created; // 冻结起始时间的毫秒值

}
