package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author RMY
 * @Date 2021/11/11 21:33
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//报告
public class Reported implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long userId;//用户Id

    private Long surveyId;//问卷id

    private Integer result;//总分

    private String conclusion;//鉴定结果

    private String cover;//结果封面

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
