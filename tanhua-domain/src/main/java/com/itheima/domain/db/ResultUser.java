package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author RMY
 * @Date 2021/11/11 21:28
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//结果和用户id表
public class ResultUser implements Serializable {

    private Long id;

    private Long userId;//用户id

    private Long surveyId;//问卷id

    private Integer resultScore;//每套题的得分

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;


}
