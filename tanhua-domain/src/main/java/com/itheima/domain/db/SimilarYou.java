package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author RMY
 * @Date 2021/11/12 20:42
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimilarYou implements Serializable {

    private Integer id;//用户id

    private String avatar;//用户头像
}
