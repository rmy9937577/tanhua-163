package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author RMY
 * @Date 2021/11/6 14:16
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Announcement implements Serializable {

    private Integer id; //主键编号
    private String title; //标题
    private String description; //描述

    @TableField(fill = FieldFill.INSERT)
    private Date created;//创建日期
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;//修改日期
}
