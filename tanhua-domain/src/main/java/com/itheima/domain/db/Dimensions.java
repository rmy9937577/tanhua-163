package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author RMY
 * @Date 2021/11/12 21:39
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dimensions implements Serializable {

    private String key;

    private String value;

}
