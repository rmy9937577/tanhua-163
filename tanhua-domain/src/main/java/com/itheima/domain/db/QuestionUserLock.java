package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author RMY
 * @Date 2021/11/13 0:35
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionUserLock implements Serializable {

    private Long id;

    private Long userId;//用户id

    private Long surveyId;//问卷id

    private Integer isLock;//对应锁状态

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
