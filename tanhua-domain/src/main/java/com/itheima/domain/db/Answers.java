package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author RMY
 * @Date 2021/11/12 15:11
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answers implements Serializable {

    private String questionId;

    private String optionId;
}
