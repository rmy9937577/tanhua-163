package com.itheima.web.listener;

import com.itheima.autoconfig.lvwang.AliyunGreenTemplate;
import com.itheima.domain.mongo.Movement;
import com.itheima.service.mongo.MovementService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovementStateListener {


    @DubboReference
    private MovementService movementService;

    @Autowired
    private AliyunGreenTemplate aliyunGreenTemplate;

    @RabbitListener(queuesToDeclare = @Queue("tanhua.movement.state"))
    public void listenMovementState(String publishId){
        System.out.println("动态审核监听："+publishId);
        // 1.查询动态详情
        Movement movement = movementService.findById(new ObjectId(publishId));

        // 2.审核
        // 2.1 文本
        Boolean checkText = aliyunGreenTemplate.checkText(movement.getTextContent());
        // 2.2 图片
        Boolean checkImage = aliyunGreenTemplate.checkImage(movement.getMedias());

        // 3. 保存审核结果
        if(checkText && checkImage){ // 通过
            movement.setState(1);
            movementService.updateState(movement);
        }else{ // 不通过
            movement.setState(2);
            movementService.updateState(movement);
        }
    }
}
