package com.itheima.web.listener;

import com.itheima.domain.db.Log;
import com.itheima.service.db.LogService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class LogListener {

    @DubboReference
    private LogService logService;

    // 日志监听器
    @RabbitListener(queuesToDeclare = @Queue("tanhua.log"))
    public void listenLog(Log log) {
        System.out.println("日志监听：" + log);
        logService.save(log);
    }
}
