package com.itheima.web.manager;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.itheima.service.db.AnalysisByDayService;
import com.itheima.vo.AnalysisSummaryVo;
import com.itheima.vo.DashboardVo;
import com.itheima.vo.LineChartVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class AnalysisManager {

    @DubboReference
    private AnalysisByDayService analysisByDayService;

    public ResponseEntity summary() {
        AnalysisSummaryVo vo = analysisByDayService.findAnalysisSummaryVo();
        return ResponseEntity.ok(vo);
    }

    // 新增、活跃用户、次日留存率
    public ResponseEntity users(Long startTime, Long endTime, String type) throws Exception {
        // 声明对象
        LineChartVo lineChartVo = new LineChartVo();
        // 调用rpc
        List<DashboardVo> thisYear = analysisByDayService.findDashboardVo(startTime, endTime, type);
        // 计算去年的时间
        long lastStartTime = DateUtil.offsetMonth(new Date(startTime), -12).getTime();
        long lastEndTime = DateUtil.offsetMonth(new Date(endTime), -12).getTime();
        // 调用rpc查询去年的
        List<DashboardVo> lastYear = analysisByDayService.findDashboardVo(lastStartTime, lastEndTime, type);
        // 封装对象
        lineChartVo.setThisYear(thisYear);
        lineChartVo.setLastYear(lastYear);

        return ResponseEntity.ok(lineChartVo);
    }

}
