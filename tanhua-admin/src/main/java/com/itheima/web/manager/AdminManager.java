package com.itheima.web.manager;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.common.utils.MD5Utils;
import com.itheima.domain.db.Admin;
import com.itheima.service.db.AdminService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.JwtUtil;
import com.itheima.web.exception.BusinessException;
import jdk.nashorn.internal.ir.IfNode;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Component
public class AdminManager {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public LineCaptcha verification(String uuid) {
        // 1.创建验证码对象
        LineCaptcha lineCaptcha = new LineCaptcha(200, 100);
        // 2.向redis存储验证码
        stringRedisTemplate.opsForValue().set(uuid, lineCaptcha.getCode(), Duration.ofMinutes(5));
        // 3.返回验证码
        return lineCaptcha;
    }

    @DubboReference
    private AdminService adminService;

    // 登录
    public ResponseEntity login(String username, String password, String verificationCode, String uuid) {
        // 1.根据uuid查询redis
        String codeFromRedis = stringRedisTemplate.opsForValue().get(uuid);
        // 2.对比验证码
        if (!StrUtil.equals(verificationCode, codeFromRedis)) {
            throw new BusinessException("验证码不匹配");
        }
        // 3.根据username查询admin对象
        Admin admin = adminService.findByUsername(username);
        if (admin == null) {
            throw new BusinessException("用户名不存在");
        }
        // 4.对比密码
        String md5Pwd = SecureUtil.md5(password);
        if (!StrUtil.equals(md5Pwd, admin.getPassword())) {
            throw new BusinessException("密码错误");
        }
        // 5.登录成功
        // 5.1清除redis中验证码
        stringRedisTemplate.delete(uuid);
        // 5.2 admin制作token
        admin.setPassword(null);
        Map<String, Object> claims = BeanUtil.beanToMap(admin);
        String token = JwtUtil.createToken(claims);
        // 5.3 向redis中存储
        String json = JSON.toJSONString(admin);
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN + token, json, Duration.ofHours(1));

        // 6.返回token
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        return ResponseEntity.ok(map);
    }

    // 获取用户基本信息
    public Admin profile(String token) {
        // 1.根据token获取redis中数据
        // 前端工程师有个习惯浏览器携带的token会加一个 Bearer （注意此处有空格）
        token = token.replaceAll("Bearer ", "");
        String json = stringRedisTemplate.opsForValue().get(ConstantUtil.ADMIN_TOKEN + token);
        if (json == null) {
            return null;
        }
        // 2.将json转为admin对象
        Admin admin = JSON.parseObject(json, Admin.class);
        // 3.续期
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN + token, json, Duration.ofHours(1));
        // 4.返回admin
        return admin;
    }

    // 管理员退出
    public void logout(String token) {
        // 1.处理token  注意：此处有空格
        token = token.replaceAll("Bearer ", "");
        // 2.redis删除
        stringRedisTemplate.delete(ConstantUtil.ADMIN_TOKEN + token);
    }
}
