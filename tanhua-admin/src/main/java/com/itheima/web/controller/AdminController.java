package com.itheima.web.controller;

import cn.hutool.captcha.LineCaptcha;
import com.itheima.domain.db.Admin;
import com.itheima.web.interceptor.AdminHolder;
import com.itheima.web.manager.AdminManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import java.io.IOException;
import java.util.Map;

@RestController
public class AdminController {

    @Autowired
    private AdminManager adminManager;

    // 生成验证码
    @GetMapping("/system/users/verification")
    public void verification(String uuid, HttpServletResponse response) throws IOException {
        // 调用manager生成验证码
        LineCaptcha lineCaptcha = adminManager.verification(uuid);
        // 通过response将验证码图片响应到客户端
        lineCaptcha.write(response.getOutputStream());
    }

    // 登录
    @PostMapping("/system/users/login")
    public ResponseEntity login(@RequestBody Map<String, String> param) {
        // 1.接收参数
        String username = param.get("username");
        String password = param.get("password");
        String verificationCode = param.get("verificationCode");
        String uuid = param.get("uuid");
        // 2.调用manager
        return adminManager.login(username, password, verificationCode, uuid);
    }

    // 获取用户基本信息
    @PostMapping("/system/users/profile")
    public ResponseEntity profile() {
        Admin admin = AdminHolder.get();
        // 返回结果
        return ResponseEntity.ok(admin);
    }

    // 管理员退出
    @PostMapping("/system/users/logout")
    public void logout(@RequestHeader("Authorization") String token) {
        // 调用manager
        adminManager.logout(token);
    }
}
