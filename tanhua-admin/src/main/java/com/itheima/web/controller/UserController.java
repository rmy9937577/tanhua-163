package com.itheima.web.controller;

import com.itheima.domain.mongo.Freeze;
import com.itheima.web.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserManager userManager;

    // 分页查询
    @GetMapping("/manage/users")
    public ResponseEntity findUserInfoByPage(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return userManager.findUserInfoByPage(pageNum, pageSize);
    }

    // 用户详情
    @GetMapping("/manage/users/{userId}")
    public ResponseEntity findUserInfoById(@PathVariable Long userId) {
        // 调用manager
        return userManager.findUserInfoById(userId);
    }

    // 动态列表
    @GetMapping("/manage/messages")
    public ResponseEntity findMovementVoByPage(
            @RequestParam(value = "state", defaultValue = "") String stateStr,
            @RequestParam(value = "uid", required = false) Long userId,
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        Integer state = null;
        if (!stateStr.equals("''") && !stateStr.equals("")) {
            state = Integer.parseInt(stateStr);
        }
        // 调用manager
        return userManager.findMovementVoByPage(state, userId, pageNum, pageSize);
    }

    // 动态详情
    @GetMapping("/manage/messages/{publishId}")
    public ResponseEntity findMovementVoById(@PathVariable String publishId) {
        // 调用manager
        return userManager.findMovementVoById(publishId);
    }


    // 动态评论列表
    @GetMapping("/manage/messages/comments")
    public ResponseEntity findCommentVoByPage(
            @RequestParam("messageID") String publishId,
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return userManager.findCommentVoByPage(publishId, pageNum, pageSize);
    }

    //查找视频
    @GetMapping("/manage/videos")
    public ResponseEntity findVideoByUserId(
            @RequestParam(value = "uid", required = false) Long userId,
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize
    ) {
        return userManager.findVideoByUserId(userId, pageNum, pageSize);
    }


    // 冻结用户
    @PostMapping("/manage/users/freeze")
    public ResponseEntity saveFreeze(@RequestBody Freeze freeze) {
        return userManager.saveFreeze(freeze);
    }

    // 解冻用户
    @PostMapping("/manage/users/unfreeze")
    public ResponseEntity unFreeze(@RequestBody Map<String,String> param){
        // 1. 获取到参数
        Integer userId = Integer.parseInt(param.get("userId"));
        String frozenRemarks = param.get("frozenRemarks");
        // 2. 调用方法
        return userManager.unFreeze(userId, frozenRemarks);
    }

    //动态复审--通过
    @PostMapping("/manage/messages/pass")
    public ResponseEntity reviewMovement(@RequestBody String[] pushIdlist){

        return userManager.reviewMovement(pushIdlist);
    }
    //动态审核--拒绝
    @PostMapping("/manage/messages/reject")
    public ResponseEntity rejectedMovement(@RequestBody String[] pushIdlist){
        return userManager.rejectedMovement(pushIdlist);
    }

}
