package com.itheima.web.controller;

import com.itheima.web.manager.AnalysisManager;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnalysisController {

    @Autowired
    private AnalysisManager analysisManager;

    // 统计展示
    @GetMapping("/dashboard/summary")
    public ResponseEntity summary() {
        return analysisManager.summary();
    }

    // 新增、活跃用户、次日留存率
    @GetMapping("/dashboard/users")
    public ResponseEntity users(@RequestParam(name = "sd") Long startTime, @RequestParam(name = "ed") Long endTime, String type) throws Exception {
        System.out.println("开始时间" + startTime + "-----------" + "结束时间" + endTime + "--------" + "type:" + type);
        // 调用manager
        return analysisManager.users(startTime, endTime, type);
    }
}
