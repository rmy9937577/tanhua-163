package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FastDFSTest {

    @Autowired // 客户端上传对象
    private FastFileStorageClient client;

    @Autowired  // 获取nginx地址
    private FdfsWebServer webServer;

    @Test
    public void test01() throws Exception {
        File file = new File("E:\\temp\\upload\\3983B21AA4CA4969B20566A6F7E89D48.jpg");
        FileInputStream inputStream = new FileInputStream(file);

        /*
            InputStream ：文件的输入流
            long ：文件大小
            String ： 文件扩展名
            Set<MetaData> ：文件描述，传入null即可
         */
        StorePath storePath = client.uploadFile(inputStream, file.length(), FileUtil.extName(file), null);

        // 组装完整url
        String fileUrl = webServer.getWebServerUrl() + storePath.getFullPath();
        System.out.println(fileUrl);
    }
}
