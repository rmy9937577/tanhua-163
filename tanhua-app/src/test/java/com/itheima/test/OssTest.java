package com.itheima.test;

import com.itheima.autoconfig.oss.OssTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OssTest {

    @Autowired
    private OssTemplate ossTemplate;

    @Test
    public void test01()throws Exception{
        String fileName= "E:\\temp\\upload\\1syn.jpg";
        FileInputStream inputStream = new FileInputStream(new File(fileName));
        String picUrl = ossTemplate.upload(fileName, inputStream);
        System.err.println(picUrl);
    }
}
