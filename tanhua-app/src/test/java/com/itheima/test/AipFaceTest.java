package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.baidu.aip.face.AipFace;
import com.baidu.aip.util.Base64Util;
import com.itheima.autoconfig.face.AipFaceTemplate;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AipFaceTest {

    @Test
    public void test01()throws Exception{
        // 初始化一个AipFace
        AipFace client = new AipFace("22959519", "OgnfdPyElBR2CS9z2NpOh2iM", "zHo2lbbjxSil8wTrzxAcH1HeWwc7VGK1");

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("face_field", "age");
        options.put("max_face_num", "2");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "NONE");

        // 将本地文件转为base64的文本格式
        String fileName= "E:\\temp\\upload\\coding.png";
        File file = new File(fileName);
        byte[] bytes = FileUtil.readBytes(file);
        String image =Base64Util.encode(bytes);
        String imageType = "BASE64";



        // 人脸检测
        JSONObject res = client.detect(image, imageType, options);
        System.out.println(res.toString(2));

    }

    @Autowired
    private AipFaceTemplate aipFaceTemplate;


    @Test
    public void test02()throws Exception{
        String fileName="E:\\temp\\upload\\11.png";
        byte[] bytes = FileUtil.readBytes(new File(fileName));
        boolean detect = aipFaceTemplate.detect(bytes);
        System.err.println(detect);
    }
}
