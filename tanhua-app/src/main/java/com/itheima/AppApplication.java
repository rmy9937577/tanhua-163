package com.itheima;

import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

// 启动排除mongo自动装配
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
@EnableCaching // 开启注解缓存
public class AppApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }

}
