package com.itheima.app.interceptor;

import com.itheima.domain.db.User;

public class UserHolder {

    // 声明threadLocal
    private static final ThreadLocal<User> TL = new ThreadLocal<>();

    // 向线程存储
    public static void set(User user) {
        TL.set(user);
    }

    // 从线程获取
    public static User get() {
        return TL.get();
    }

    // 从线程删除
    public static void remove() {
        TL.remove();
    }
}
