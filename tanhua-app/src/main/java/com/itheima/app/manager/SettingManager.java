package com.itheima.app.manager;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.BlackList;
import com.itheima.domain.db.Notification;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.UserInfo;
import com.itheima.service.db.BlackListService;
import com.itheima.service.db.NotificationService;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SettingVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SettingManager {

    @DubboReference
    private QuestionService questionService;

    @DubboReference
    private NotificationService notificationService;

    // 查询通用设置
    public ResponseEntity findSettingVo() {
        // 1.从线程内获取user对象
        Long userId = UserHolder.get().getId();
        // 2.查询陌生人问题
        Question question = questionService.findByUserId(userId);
        // 3.查询推送通知
        Notification notification = notificationService.findByUserId(userId);
        // 4.封装settingVo
        SettingVo settingVo = new SettingVo();
        settingVo.setId(userId); // 用户id
        settingVo.setPhone(UserHolder.get().getPhone()); // 用户手机号
        // 用户陌生人问题
        if (question != null) {
            settingVo.setStrangerQuestion(question.getStrangerQuestion());
        }
        // 用户推送通知
        if (notification != null) {
            settingVo.setLikeNotification(notification.getLikeNotification());
            settingVo.setPinglunNotification(notification.getPinglunNotification());
            settingVo.setGonggaoNotification(notification.getGonggaoNotification());
        }
        // 5.返回vo
        return ResponseEntity.ok(settingVo);
    }

    // 设置陌生人问题
    public void setQuestion(String content) {
        // 1.获取线程内用户id
        Long userId = UserHolder.get().getId();
        // 2.查询此用户陌生人问题
        Question question = questionService.findByUserId(userId);
        // 3.判断
        if (question == null) {// 新增
            question = new Question();
            question.setUserId(userId);
            question.setStrangerQuestion(content);
            questionService.save(question);
        } else { // 修改
            question.setStrangerQuestion(content);
            questionService.update(question);
        }
    }

    // 设置推送通知
    public void setNotification(Notification param) {
        // 1.获取线程内用户id
        Long userId = UserHolder.get().getId();
        // 2.根据用户id查询推送通知
        Notification notification = notificationService.findByUserId(userId);
        // 3.判断
        if (notification == null) { // 新增
            param.setUserId(userId);
            notificationService.save(param);
        } else { // 更新
            notification.setLikeNotification(param.getLikeNotification());
            notification.setPinglunNotification(param.getPinglunNotification());
            notification.setGonggaoNotification(param.getGonggaoNotification());
            notificationService.update(notification);
        }
    }

    @DubboReference
    private BlackListService blackListService;

    @DubboReference
    private UserInfoService userInfoService;

    // 黑名单查询
    public ResponseEntity findBlackListByPage(Integer pageNum, Integer pageSize) {
        // 1.获取线程内用户id
        Long userId = UserHolder.get().getId();
        // 2.调用rpc分页查询黑名单
        PageBeanVo pageBeanVo = blackListService.findByPage(userId, pageNum, pageSize);
        // 3.封装用户详情
        // 3.1数据来源
        List<BlackList> blackListList = (List<BlackList>) pageBeanVo.getItems();
        // 3.2封装实体
        List<UserInfo> userInfoList = new ArrayList<>();
        // 3.3遍历
        if (blackListList != null && blackListList.size() > 0) {
            for (BlackList blackList : blackListList) {
                // 获取黑名单用户id
                Long blackUserId = blackList.getBlackUserId();
                // 查询用户详情
                UserInfo userInfo = userInfoService.findUserInfoById(blackUserId);
                // 添加到集合中
                userInfoList.add(userInfo);
            }
        }
        // 4.将userInfo设置到pageBeanVo中
        pageBeanVo.setItems(userInfoList);
        // 5.返回vo
        return ResponseEntity.ok(pageBeanVo);
    }

    // 移除黑名单
    public void deleteBalckList(Long blackUserId) {
        // 1.获取线程内用户id
        Long userId = UserHolder.get().getId();
        // 2.调用rpc删除
        blackListService.deleteByCondition(userId, blackUserId);
    }
}
