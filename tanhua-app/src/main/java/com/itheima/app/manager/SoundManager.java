package com.itheima.app.manager;

import cn.hutool.core.io.FileUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.itheima.domain.mongo.Sound;
import com.itheima.service.mongo.SoundService;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.SoundVo;
import org.apache.dubbo.config.annotation.DubboReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class SoundManager {

    @DubboReference
    private SoundService soundService;
    @DubboReference
    private UserInfoService userInfoService;
    @Autowired
    private FastFileStorageClient client;

    @Autowired
    private FdfsWebServer webServer;

    //发送语音
    public void saveSound(Long userId, MultipartFile soundFile) throws Exception {
        //1.上传语音
        StorePath storePath = client.uploadFile(soundFile.getInputStream(), soundFile.getSize(), FileUtil.extName(soundFile.getOriginalFilename()), null);
        String soundUrl = webServer.getWebServerUrl() + storePath.getFullPath();

        //2.封装对象
        Sound sound = new Sound();
        sound.setUserId(userId);
        sound.setSoundUrl(soundUrl);
        sound.setCreated(System.currentTimeMillis());
        //调用rpc保存
        soundService.saveSound(sound);

    }



    //接收语音
    public SoundVo findSound(Long userId) {
        SoundVo sound = soundService.findSound(userId);
        return sound;
    }



}
