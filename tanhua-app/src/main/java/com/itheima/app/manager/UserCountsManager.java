package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;

import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Visitor;
import com.itheima.service.mongo.UserCountsSrevice;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.UserLikeVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UserCountsManager {


    @DubboReference
    private UserCountsSrevice userCountsSrevice;
    @DubboReference
    private UserInfoService userInfoService;
   private  HuanXinTemplate huanXinTemplate;
    //喜欢，粉丝，互相喜欢数量
    public ResponseEntity userLikeEachOther() {
        //线程中获得用户id
        Long userId = UserHolder.get().getId();
        //调用rpc
        Map map = userCountsSrevice.userLikeEachOther(userId);

       return ResponseEntity.ok(map);

    }


    //喜欢，粉丝，互相喜欢详情
    public ResponseEntity LikeDetails(String type, Integer pageNum, Integer pageSize) {
        //线程中获得用户id
        Long userId = UserHolder.get().getId();
        //type转int
        int i = Integer.parseInt(type);

        //调用rpc
        PageBeanVo pageBeanVo = userCountsSrevice.likeDetails(i, userId, pageNum, pageSize);
        //声明
        List <UserLikeVo> userlist=new ArrayList<>();
        //获得喜欢的用户详情
      if (i==4){
          List<Visitor> items = (List<Visitor>) pageBeanVo.getItems();
          if (CollectionUtil.isNotEmpty(items)) {
              for (Visitor item : items) {
                  UserLikeVo userLikeVo = new UserLikeVo();
                  UserInfo userInfoById = userInfoService.findUserInfoById(item.getVisitorUserId());
                  //封装
                  userLikeVo.setId(userInfoById.getId());//id
                  userLikeVo.setAvatar(userInfoById.getAvatar());//头像
                  userLikeVo.setNickname(userInfoById.getNickname());//昵称
                  userLikeVo.setGender(userInfoById.getGender());//性别
                  userLikeVo.setAge(userInfoById.getAge());//年龄
                  userLikeVo.setCity(userInfoById.getCity());//城市
                  userLikeVo.setEducation(userInfoById.getEducation());//学历
                  userLikeVo.setMarriage(userInfoById.getMarriage());//婚姻状况
                  userLikeVo.setMatchRate(RandomUtil.randomInt(33,99));
                  userLikeVo.setAlreadyLove(false);

                  userlist.add(userLikeVo);
              }

          }
      }else {
          List<Long> list = (List<Long>) pageBeanVo.getItems();
          if (CollectionUtil.isNotEmpty(list)) {
              for (Long aLong : list) {

                  UserLikeVo userLikeVo = new UserLikeVo();
                  UserInfo userInfoById = userInfoService.findUserInfoById(aLong);
                  //封装
                  userLikeVo.setId(userInfoById.getId());//id
                  userLikeVo.setAvatar(userInfoById.getAvatar());//头像
                  userLikeVo.setNickname(userInfoById.getNickname());//昵称
                  userLikeVo.setGender(userInfoById.getGender());//性别
                  userLikeVo.setAge(userInfoById.getAge());//年龄
                  userLikeVo.setCity(userInfoById.getCity());//城市
                  userLikeVo.setEducation(userInfoById.getEducation());//学历
                  userLikeVo.setMarriage(userInfoById.getMarriage());//婚姻状况
                  userLikeVo.setMatchRate(RandomUtil.randomInt(33,99));
                  userLikeVo.setAlreadyLove(false);

                  userlist.add(userLikeVo);
              }

          }
      }


        pageBeanVo.setItems(userlist);
        return ResponseEntity.ok(pageBeanVo);

    }

//    //喜欢
//    public void someLike(Long userId, String uid) {
//        long xihuanId = Long.parseLong(uid);
//        Integer i = userCountsSrevice.SomeLike(userId, xihuanId);
//        //判断
//        if(i==1){
//            huanXinTemplate.addContacts("HX"+userId,"HX"+xihuanId);
//        }else {
//
//        }
//    }
//    //取消喜欢
//    public void deleteLike(Long userId, String uid) {
//        long xinhuanId = Long.parseLong(uid);
//        Integer i = userCountsSrevice.deleteLike(userId, xinhuanId);
//        if (i==1){
//            huanXinTemplate.deleteContacts("HX"+userId,"HX"+uid);
//        }
//
//    }
}
