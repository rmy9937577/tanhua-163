package com.itheima.app.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.FriendService;
import com.itheima.service.mongo.SlideCardsService;
import com.itheima.vo.SlideCardsVo;
import com.itheima.vo.UserInfoVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SlideCardsManager {


    @DubboReference
    private SlideCardsService slideCardsService;

    @DubboReference
    private UserInfoService userInfoService;

    // 探花分页查询推荐
    public ResponseEntity slideCards() {
        // 1. 取出线程中的id
        Long toUserId = UserHolder.get().getId();
        // 2. 调用rpc查询
        List<RecommendUser> recommendUsers = slideCardsService.findRecommendUser(toUserId);
        // 3. 进行判空和遍历
        // 3.1 声明一个set集合 为了让集合中的数据无序随机
        Set<SlideCardsVo> slideCardsVos = new HashSet<>();
        if (CollectionUtil.isNotEmpty(recommendUsers)) {
            for (RecommendUser recommendUser : recommendUsers) {
                // 4. 取出推荐用户id
                Long userId = recommendUser.getUserId();
                // 5. 查询用户信息
                UserInfo userInfo = userInfoService.findUserInfoById(userId);
                // 6. 创建vo对象并封装
                SlideCardsVo vo = new SlideCardsVo();
                vo.setUserInfo(userInfo);
                // 7. 添加进set集合
                slideCardsVos.add(vo);
            }
            // 9. 返回
            return ResponseEntity.ok(slideCardsVos);
        }
        return ResponseEntity.status(500).body("大数据还未给该您推荐用户！");
    }


    @DubboReference
    private FriendService friendService;

    @Autowired
    private HuanXinTemplate huanXinTemplate;


    // 探花-喜欢
    public void saveLove(Long likeUserId) {
        // 1. 取出线程中的userId
        Long userId = UserHolder.get().getId();
        // 2. 创建userLike对象并封装
        UserLike userLike = new UserLike();
        userLike.setUserId(userId);
        userLike.setLikeUserId(likeUserId);
        userLike.setCreated(System.currentTimeMillis());
        // 3. 调用rpc保存并判断对方是否喜欢着你
        Boolean saveLove = slideCardsService.saveLove(userLike);
        // 4. 判断
        if (saveLove) {
            // 5. 说明对方也喜欢自己  所以调用rpc加好友
            friendService.addContacts(userId, likeUserId);
            // 6. 加环信好友
            huanXinTemplate.addContacts("HX" + userId, "HX" + likeUserId);
        }
        // 7. 删除掉推荐列表内的数据
        slideCardsService.remove(userId, likeUserId);
    }

    // 探花-不喜欢
    public void removeLove(Long likeUserId) {
        // 1. 获取线程中的userId
        Long userId = UserHolder.get().getId();
        // 2. 调用rpc删除喜欢  并判断对方是否喜欢自己
        Boolean removeLove = slideCardsService.removeLove(userId, likeUserId);
        // 3. 判断
        if (removeLove) {
            // 4. 说明对方喜欢着自己 删除喜欢时候也要删除掉好友关系
            friendService.removeContacts(userId, likeUserId);
            // 5. 删除掉环信上的好友
            huanXinTemplate.deleteContacts("HX" + userId, "HX" + likeUserId);
        }
        // 6. 删除推荐列表内的数据
        slideCardsService.remove(userId, likeUserId);
    }
}
