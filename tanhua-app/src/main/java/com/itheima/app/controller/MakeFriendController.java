package com.itheima.app.controller;

import com.itheima.app.manager.MakeFriendManager;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.util.Map;

@RestController
public class MakeFriendController {

    @Autowired
    private MakeFriendManager makeFriendManager;

    // 今日佳人
    @GetMapping("/tanhua/todayBest")
    public ResponseEntity findTodayBest() {
        return makeFriendManager.findTodayBest();
    }

    // 推荐用户
    @GetMapping("/tanhua/recommendation")
    public ResponseEntity findRecommendUserList(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return makeFriendManager.findRecommendUserList(pageNum, pageSize);
    }

    // 用户详情封面
    @GetMapping("/tanhua/{jiarenId}/personalInfo")
    public ResponseEntity findRecommendDetailHead(@PathVariable Long jiarenId) {
        return makeFriendManager.findRecommendDetailHead(jiarenId);
    }

    // 是否喜欢他（她）
    @GetMapping("/users/{likeUserId}/alreadyLove")
    public ResponseEntity findAlreadyLove(@PathVariable Long likeUserId) {
        return makeFriendManager.findAlreadyLove(likeUserId);
    }

    // 历史访客
    @GetMapping("/movements/visitors")
    public ResponseEntity findVisitors() {
        return makeFriendManager.findVisitors();
    }

    // 上传地址位置
    @PostMapping("/baidu/location")
    public void uploadLocation(@RequestBody Map<String, String> param) {
        System.out.println("上传地理位置：" + param);
        // 1.接收参数
        Double longitude = Double.parseDouble(param.get("longitude"));
        Double latitude = Double.parseDouble(param.get("latitude"));
        String addrStr = param.get("addrStr");
        // 2.调用manager
        makeFriendManager.uploadLocation(longitude, latitude, addrStr);

    }

    // 搜附近
    @GetMapping("/tanhua/search")
    public ResponseEntity searchNearUser(String gender, Long distance) {
        return makeFriendManager.searchNearUser(gender, distance);
    }

    // 聊一下
    @GetMapping("/tanhua/strangerQuestions")
    public ResponseEntity strangerQuestions(@RequestParam("userId") Long jiarenId) {
        return makeFriendManager.strangerQuestions(jiarenId);
    }


}
