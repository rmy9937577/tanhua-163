package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.VideoManager;
import com.itheima.vo.PageBeanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.PATCH;
import java.util.Map;

@RestController
public class VideoController {

    @Autowired
    private VideoManager videoManager;

    // 视频分页查询
    @GetMapping("/smallVideos")
    public ResponseEntity findVideoVoByPage(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 获取线程内userId
        Long userId = UserHolder.get().getId();
        // 调用manager
        PageBeanVo pageBeanVo = videoManager.findVideoVoByPage(userId, pageNum, pageSize);
        // 返回
        return ResponseEntity.ok(pageBeanVo);
    }

    // 发布视频
    @PostMapping("/smallVideos")
    public void saveVideo(MultipartFile videoThumbnail, MultipartFile videoFile) throws Exception {
        // 获取登录用户id
        Long userId = UserHolder.get().getId();
        // 调用manager
        videoManager.saveVideo(userId, videoThumbnail, videoFile);
    }


    // 关注
    @PostMapping("/smallVideos/{focusUserId}/userFocus")
    public void saveFocusUser(@PathVariable Long focusUserId){
        // 获取用户id
        Long userId = UserHolder.get().getId();
        // 调用manager
        videoManager.saveFocusUser(userId,focusUserId);
    }

    // 取消关注
    @PostMapping("/smallVideos/{focusUserId}/userUnFocus")
    public void removeFocusUser(@PathVariable Long focusUserId){
        // 获取用户id
        Long userId = UserHolder.get().getId();
        // 调用manager
        videoManager.removeFocusUser(userId,focusUserId);
    }
    //视频点赞
    @PostMapping("/smallVideos/{id}/like")
    public void likeVideos(@PathVariable String id) {
        Long userId = UserHolder.get().getId();
        videoManager.likeVideos(id, userId,1);
    }

    //视频取消喜欢
    @PostMapping("/smallVideos/{id}/dislike")
    public void dislikeVideos(@PathVariable String id) {
        Long userId = UserHolder.get().getId();
        videoManager.likeVideos(id, userId,2);
    }

    //视频评论
    @PostMapping("/smallVideos/{id}/comments")
    public void commentVideo(@PathVariable(value = "id") String videoId,
                             @RequestBody Map<String,String> param) {
        String content = param.get("comment");
        Long userId = UserHolder.get().getId();
        videoManager.commentVideo(userId, videoId, content);
    }

    //查询评论列表
    @GetMapping("/smallVideos/{id}/comments")
    public ResponseEntity findCommentPage(@RequestParam(value = "page",defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        Long userId = UserHolder.get().getId();
        return videoManager.findCommentPage(pageNum, pageSize, userId);
    }
}
