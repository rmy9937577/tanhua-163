package com.itheima.app.controller;

import com.itheima.app.manager.SettingManager;
import com.itheima.domain.db.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import java.util.Map;

@RestController
public class SettingController {

    @Autowired
    private SettingManager settingManager;

    // 查询通用设置
    @GetMapping("/users/settings")
    public ResponseEntity findSettingVo() {
        // 调用manager
        return settingManager.findSettingVo();
    }

    // 设置陌生人问题
    @PostMapping("/users/questions")
    public void setQuestion(@RequestBody Map<String, String> param) {
        // 接收参数
        String content = param.get("content");
        // 调用manager
        settingManager.setQuestion(content);
    }

    // 设置推送通知
    @PostMapping("/users/notifications/setting")
    public void setNotification(@RequestBody Notification param) {
        // 调用manager
        settingManager.setNotification(param);
    }

    // 黑名单查询
    @GetMapping("/users/blacklist")
    public ResponseEntity findBlackListByPage(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return settingManager.findBlackListByPage(pageNum, pageSize);
    }

    // 移除黑名单
    @DeleteMapping("/users/blacklist/{uid}")
    public void deleteBalckList(@PathVariable("uid") Long blackUserId) {
        // 调用manager
        settingManager.deleteBalckList(blackUserId);
    }
}
