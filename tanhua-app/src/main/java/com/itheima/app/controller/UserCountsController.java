package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.SlideCardsManager;
import com.itheima.app.manager.UserCountsManager;
import org.apache.dubbo.remoting.exchange.support.header.ReconnectTimerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserCountsController {

    @Autowired
   private UserCountsManager userCountsManager;
    @Autowired
    private SlideCardsManager slideCardsManager;
    //喜欢，粉丝，互相喜欢数量
    @GetMapping("/users/counts")
    public ResponseEntity userLikeEachOther(){



      return   userCountsManager.userLikeEachOther();
    }

    //喜欢，粉丝，互相喜欢详情
    @GetMapping("/users/friends/{type}")
    public ResponseEntity LikeDetails(@PathVariable String type,
                                      @RequestParam(value = "page",defaultValue = "1") Integer pageNum,
                                      @RequestParam(value = "pagesize",defaultValue = "10")Integer pageSize){
        return userCountsManager.LikeDetails(type,pageNum,pageSize);
    }
    //喜欢
     @PostMapping("/users/fans/{uid}")
    public void someLike(@PathVariable String uid){
        //线程中获得用户id
         long likeUserId = Long.parseLong(uid);
         slideCardsManager.saveLove(likeUserId);
     }
        //取消喜欢
    @DeleteMapping("/users/like/{uid}")
    public  void deleteLike(@PathVariable String uid){
        long likeUserId = Long.parseLong(uid);
        slideCardsManager.removeLove(likeUserId);
    }

}
