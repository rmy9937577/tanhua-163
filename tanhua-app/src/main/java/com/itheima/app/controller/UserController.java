package com.itheima.app.controller;

import com.itheima.app.manager.UserManager;
import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.GET;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserManager userManager;

    // 保存用户
    @PostMapping("/user/save")
    public ResponseEntity save(@RequestBody User user) {
        return userManager.save(user);
    }

    // 查询用户
    @GetMapping("/user/findByPhone")
    public ResponseEntity findByPhone(String phone) {
        return userManager.findByPhone(phone);
    }

    // 发送短信验证码
    @PostMapping("/user/login")
    public void sendSms(@RequestBody Map<String, String> param) {
        // 获取手机号
        String phone = param.get("phone");
        // 调用manager
        userManager.sendSms(phone);
    }

    // 注册、登录功能
    @PostMapping("/user/loginVerification")
    public ResponseEntity loginAndRegister(@RequestBody Map<String, String> param) {
        // 1.接收请求参数
        String phone = param.get("phone");
        String verificationCode = param.get("verificationCode");
        // 2.调用manager
        return userManager.loginAndRegister(phone, verificationCode);
    }

    // 完善用户基础信息
    @PostMapping("/user/loginReginfo")
    public void saveUserInfoBase(@RequestBody UserInfo userInfo, @RequestHeader("Authorization") String token) {
        // 调用manager
        userManager.saveUserInfoBase(userInfo,token);
    }

    // 完善用户头像信息
    @PostMapping({"/user/loginReginfo/head","/users/header"})
    public ResponseEntity saveUserInfoHead(MultipartFile headPhoto, @RequestHeader("Authorization") String token)throws Exception{
        // 调用manager
       return userManager.saveUserInfoHead(headPhoto,token);
    }



    // 查询用户信息
    @GetMapping("/users")
    public ResponseEntity findUserInfoVo(Long userID,Long huanxinID, @RequestHeader("Authorization") String token){
        if (userID!=null) {
            return userManager.findUserInfoVo(userID);
        }else if(huanxinID!=null){
            return userManager.findUserInfoVo(huanxinID);
        }else{
            User user = userManager.findUserByToken(token);
            return userManager.findUserInfoVo(user.getId());
        }
    }


    // 修改用户基础信息
    @PutMapping("/users")
    public void updateUserInfoBase(@RequestBody UserInfo userInfo, @RequestHeader("Authorization") String token){
        // 调用manager
        userManager.updateUserInfoBase(userInfo,token);
    }
    //发送验证码
    @PostMapping("/users/phone/sendVerificationCode")
    public void sendVerificationCode() {
        userManager.sendVerificationCode();
    }


    //效验验证码
    @PostMapping("/users/phone/checkVerificationCode")
    public ResponseEntity checkVerificationCode(@RequestBody Map<String,String>param) {

        String verificationCode = param.get("verificationCode");
        return userManager.checkVerificationCode(verificationCode);
    }


    //更改手机号
    @PostMapping("/users/phone")
    public void updatePhone(@RequestBody Map<String,String>param,@RequestHeader("Authorization")String authorization) {
        String phone = param.get("phone");
        userManager.updatePhone(phone,authorization);
    }
}
