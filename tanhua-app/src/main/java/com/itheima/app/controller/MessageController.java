package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.MessageManager;
import com.itheima.vo.HuanXinVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.util.Map;

@RestController
public class MessageController {

    // 获取app端的环信账号
    @GetMapping("/huanxin/user")
    public ResponseEntity huanxinUser() {

        // 创建环信vo
        HuanXinVo huanXinVo = new HuanXinVo();
        huanXinVo.setUsername("HX" + UserHolder.get().getId());
        huanXinVo.setPassword("123456");

        return ResponseEntity.ok(huanXinVo);
    }

    @Autowired
    private MessageManager messageManager;

    // 打招呼（好友申请）
    @PostMapping("/tanhua/strangerQuestions")
    public void strangerQuestions(@RequestBody Map<String, String> param) {
        // 1.接收参数
        Long jiarenId = Long.parseLong(param.get("userId"));
        String reply = param.get("reply");

        // 2.调用manager
        messageManager.strangerQuestions(jiarenId, reply);
    }

    // 添加好友（联系人）
    @PostMapping("/messages/contacts")
    public void addContacts(@RequestBody Map<String, Long> param) {
        // 1.获取好友id
        Long friendId = param.get("userId");
        // 2.调用manager
        messageManager.addContacts(friendId);
    }

    // 查看联系人
    @GetMapping("/messages/contacts")
    public ResponseEntity findContactsByPage(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        return messageManager.findContactsByPage(pageNum, pageSize);
    }

    // 根据环信id查询用户信息
    @GetMapping("/messages/userinfo")
    public ResponseEntity findUserInfoByHuanXinId(String huanxinId) {
        // 调用manager查询
        return messageManager.findUserInfoByHuanXinId(huanxinId);
    }

    // 查询点赞用户
    @GetMapping("/messages/likes")
    public ResponseEntity findLikes(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return messageManager.findUserCommentVoByPage(1, pageNum, pageSize);
    }

    // 查询评论用户
    @GetMapping("/messages/comments")
    public ResponseEntity findComments(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return messageManager.findUserCommentVoByPage(2, pageNum, pageSize);
    }
    // 查询喜欢用户
    @GetMapping("/messages/loves")
    public ResponseEntity findLoves(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        // 调用manager
        return messageManager.findUserCommentVoByPage(3, pageNum, pageSize);
    }

    //查询所有公告
    @GetMapping("/messages/announcements")
    public ResponseEntity findAllAnnouncement(@RequestParam(value = "page",defaultValue = "1")Integer pageNum,@RequestParam(value = "pagesize",defaultValue = "10")Integer pageSize){
        //调用Manager
        return messageManager.findAllAnnouncement(pageNum,pageSize);
    }
}
