package com.itheima.app.controller;

import com.itheima.app.manager.MovementManager;
import com.itheima.domain.mongo.Movement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.GET;
import java.util.Map;

@RestController
public class MovementController {

    @Autowired
    private MovementManager movementManager;

    // 发布动态
    @PostMapping("/movements")
    public ResponseEntity saveMovement(Movement movement, MultipartFile[] imageContent) throws Exception {
        return movementManager.saveMovement(movement, imageContent);
    }

    // 查看个人动态
    @GetMapping("/movements/all")
    public ResponseEntity findMyMovement(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
            Long userId) {
        return movementManager.findMyMovement(userId, pageNum, pageSize);
    }

    // 查看好友动态
    @GetMapping("/movements")
    public ResponseEntity findFriendMovement(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        return movementManager.findFriendMovement(pageNum, pageSize);
    }

    // 查看推荐动态
    @GetMapping("/movements/recommend")
    public ResponseEntity findRecommendMovement(
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        return movementManager.findRecommendMovement(pageNum, pageSize);
    }

    // 动态点赞
    @GetMapping("/movements/{publishId}/like")
    public ResponseEntity setMovementLike(@PathVariable String publishId) {
        return movementManager.setMovementLike(publishId);
    }

    // 动态取消点赞
    @GetMapping("/movements/{publishId}/dislike")
    public ResponseEntity removeMovementLike(@PathVariable String publishId) {
        return movementManager.removeMovementLike(publishId);
    }

    // 动态喜欢
    @GetMapping("/movements/{publishId}/love")
    public ResponseEntity setMovementLove(@PathVariable String publishId) {
        return movementManager.setMovementLove(publishId);
    }

    // 动态取消喜欢
    @GetMapping("/movements/{publishId}/unlove")
    public ResponseEntity removeMovementLove(@PathVariable String publishId) {
        return movementManager.removeMovementLove(publishId);
    }

    // 查看单条动态
    @GetMapping("/movements/{publishId}")
    public ResponseEntity findeMovemetnVoDetail(@PathVariable String publishId) {
        return movementManager.findeMovemetnVoDetail(publishId);
    }

    // 查看动态评论列表
    @GetMapping("/comments")
    public ResponseEntity findCommentVo(
            @RequestParam("movementId") String publishId,
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        return movementManager.findCommentVo(publishId, pageNum, pageSize);
    }


    // 发表评论
    @PostMapping("/comments")
    public ResponseEntity setMovementComment(@RequestBody Map<String,String> param){
        // 1.接收参数
        String publishId = param.get("movementId");
        String content = param.get("comment");
        // 2 .调用manager
        return movementManager.setMovementComment(publishId,content);
    }
}
