package com.itheima.app.controller;


import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.SoundManager;

import com.itheima.service.mongo.SoundService;
import com.itheima.vo.SoundVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SoundController {

    @Autowired
    private SoundManager soundManager;
    @DubboReference
    private SoundService soundService;
    //发送语音
    @PostMapping("/peachblossom")
    public void saveSound(@RequestBody MultipartFile soundFile)throws Exception{
        //1.获取用户id
        Long userId = UserHolder.get().getId();
        //调用manager
        soundManager.saveSound(userId, soundFile);
    }

    //接收语音
    @GetMapping("/peachblossom")
    public ResponseEntity findSound(){
        Long userId = UserHolder.get().getId();
        SoundVo soundVo = soundManager.findSound(userId);
        return ResponseEntity.ok(soundVo);
    }

    //喜欢


}
