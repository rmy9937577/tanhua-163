package com.itheima.app.controller;

import com.itheima.app.manager.TestSoulManager;
import com.itheima.domain.db.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author RMY
 * @Date 2021/11/12 9:31
 * @Version 1.0
 */
//测灵魂
@RestController
public class TestSoulController {
    @Autowired
    private TestSoulManager testSoulManager;

    @GetMapping("/testSoul")
    //app端查询所有题库
    public ResponseEntity findAllSurvey(){
        return testSoulManager.findAllSurvey();
    }


    @PostMapping("/testSoul")
    //接受答题的题号和选择号,并结算结果，把reportedId返回
    public ResponseEntity computeAndReturnReportedId(@RequestBody Map<String, List<Answers>> param){
        List<Answers> answers = param.get("answers");
        //将前端收到的集合传到去
        return testSoulManager.computeAndReturnReportedId(answers);
    }

    @GetMapping("/testSoul/report/{reportedId}")
    //查看自己的结果
    public ResponseEntity findReportedById(@PathVariable String reportedId){
        //调用testSoulManager
        return testSoulManager.findReportedById(reportedId);
    }

}
