package com.itheima.app.controller;

import com.itheima.app.manager.SlideCardsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// 探花交友模块
@RestController
public class SlideCardsController {

    @Autowired
    private SlideCardsManager slideCardsManager;

    // 探花分页查询推荐
    @GetMapping("/tanhua/cards")
    public ResponseEntity slideCards() {
        // 调用manager
        return slideCardsManager.slideCards();
    }

    // 探花-喜欢
    @GetMapping("/tanhua/{likeUserId}/love")
    public void saveLove(@PathVariable Long likeUserId) {
        // 调用manager
        slideCardsManager.saveLove(likeUserId);
    }

    // 探花-不喜欢
    @GetMapping("/tanhua/{likeUserId}/unlove")
    public void removeLove(@PathVariable Long likeUserId) {
        // 调用manager
        slideCardsManager.removeLove(likeUserId);
    }

}
