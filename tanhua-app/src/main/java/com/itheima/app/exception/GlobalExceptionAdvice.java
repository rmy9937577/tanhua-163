package com.itheima.app.exception;

import com.itheima.vo.ErrorResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(Exception.class)
    public ResponseEntity handlerExceptionMethod(Exception ex){
        // 将异常信息输出到控制台
        ex.printStackTrace();

        // 给前端友好提示
        return ResponseEntity.status(500).body(ErrorResult.error());
    }
}
