package com.itheima.util;

/**
 * @Author RMY
 * @Date 2021/11/12 16:38
 * @Version 1.0
 */
public class TestSoulUtil {
    //灵魂基因鉴定单
    //猫头鹰
    public static final String maoTouYing = "猫头鹰：他们的共同特质为重计划、条理、细节精准。在行为上，表现出喜欢理性思考与分析、较重视制度、结构、规范。他们注重执行游戏规则、循规蹈矩、巨细靡遗、重视品质、敬业负责。,白兔型：平易近人、敦厚可靠、避免冲突与不具批判性。在行为上，表现出不慌不忙、冷静自持的态度。他们注重稳定与中长程规划，现实生活中，常会反思自省并以和谐为中心，即使面对困境，亦能泰然自若，从容应付。";
    //狐狸型
    public static final String huLi = "狐狸型 ：人际关系能力极强，擅长以口语表达感受而引起共鸣，很会激励并带动气氛。他们喜欢跟别人互动，重视群体的归属感，基本上是比较「人际导向」。由于他们富同理心并乐于分享，具有很好的亲和力，在服务业、销售业、传播业及公共关系等领域中，狐狸型的领导者都有很杰出的表现。";
    //狮子型
    public static final String shiZi = "狮子型：性格为充满自信、竞争心强、主动且企图心强烈，是个有决断力的领导者。一般而言，狮子型的人胸怀大志，勇于冒险，看问题能够直指核心，并对目标全力以赴。他们在领导风格及决策上，强调权威与果断，擅长危机处理，此种性格最适合开创性与改革性的工作。";

    //维度项
    public static final String WAI_XIANG = "外向";
    public static final String PAN_DUAN = "判断";
    public static final String CHOU_XIANG = "抽象";
    public static final String LI_XING = "理性";

    //维度值
    public static final String SIXTY = "60%";
    public static final String SEVENTY = "70%";
    public static final String EIGHTY = "80%";
    public static final String NINETY = "90%";

    //封面
    public static final String maoTouYingPic = "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/owl.png";
    public static final String huLiPic = "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/fox.png";
    public static final String shiZiPic = "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/lion.png";
}
