package com.itheima.job;

import com.itheima.service.db.UserInfoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class SoundJob {


    @DubboReference
    private UserInfoService userInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Scheduled(cron = "0 0 0/24 * * ?")
    //@Scheduled(cron = "0/30 * * * * ?")
    public void clearSoundJob(){
        Set<String> keys = stringRedisTemplate.keys("remainingTimes_*");
        for (String key : keys) {
            stringRedisTemplate.opsForValue().set(key,"10");
        }
    }
}
