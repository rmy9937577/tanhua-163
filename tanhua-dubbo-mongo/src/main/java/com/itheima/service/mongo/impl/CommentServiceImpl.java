package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.service.mongo.CommentService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class CommentServiceImpl implements CommentService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Integer saveComment(Comment comment) {
        // 1.保存comment对象
        mongoTemplate.save(comment);
        // 2.根据commentType类型操作动态详情数量
        switch (comment.getCommentType()) {
            case 1: { // 动态点赞
                // 查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 数量+1
                movement.setLikeCount(movement.getLikeCount() + 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getLikeCount();
            }
            case 2: { // 动态评论
                // 查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 数量+1
                movement.setCommentCount(movement.getCommentCount() + 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getCommentCount();
            }
            case 3: { // 动态喜欢
                // 查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 数量+1
                movement.setLoveCount(movement.getLoveCount() + 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getLoveCount();
            }
        }
        return 0;
    }

    @Override
    public Integer removeComment(ObjectId publishId, Long userId, Integer commentType) {
        // 1.构建三个条件删除
        Query query = new Query(
                Criteria.where("publishId").is(publishId) // 发布id
                        .and("userId").is(userId)  // 登录人
                        .and("commentType").is(commentType) // 操作类型
        );
        mongoTemplate.remove(query, Comment.class);
        // 2.根据commentType操作动态详情数量-1
        switch (commentType) {
            case 1: { // 动态点赞
                // 先查动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 数量-1
                movement.setLikeCount(movement.getLikeCount() - 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getLikeCount();
            }
            case 2: { // 动态评论
                // 先查动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 数量-1
                movement.setCommentCount(movement.getCommentCount() - 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getCommentCount();
            }
            case 3: { // 动态喜欢
                // 先查动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 数量-1
                movement.setLoveCount(movement.getLoveCount() - 1);
                // 更新
                mongoTemplate.save(movement);
                // 返回数量
                return movement.getLoveCount();
            }
        }
        return 0;
    }

    // 分页查询评论
    @Override
    public PageBeanVo findByPage(ObjectId publishId, Integer commentType, Integer pageNum, Integer pageSize) {
        // 1.构建查询条件
        Query query = new Query(  // 条件
                Criteria.where("publishId").is(publishId)
                        .and("commentType").is(commentType)
        ).with(Sort.by(Sort.Order.desc("created"))) // 排序
                .skip((pageNum - 1) * pageSize).limit(pageSize); // 分页
        // 2.查询comment集合
        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        // 3.查询总记录数
        long total = mongoTemplate.count(query, Comment.class);
        // 4.返回并封装分页对象
        return new PageBeanVo(pageNum, pageSize, total, commentList);
    }

    // 查看点赞、评论、喜欢的用户信息
    @Override
    public PageBeanVo findCommentTypeByPage(Long publishUserId, Integer commentType, Integer pageNum, Integer pageSize) {
        // 1.构建条件
        Query query = Query.query(Criteria.where("publishUserId").is(publishUserId).and("commentType").is(commentType))
                .with(Sort.by(Sort.Order.desc("created")))
                .skip((pageNum - 1) * pageSize).limit(pageSize);
        // 2.查询list
        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        // 3.查询total
        long total = mongoTemplate.count(query, Comment.class);
        // 4.返回并封装分页对象
        return new PageBeanVo(pageNum, pageSize, total, commentList);
    }

    //查询单条评论
    @Override
    public Comment findCommentById(String commentId) {
        Comment comment = mongoTemplate.findById(commentId, Comment.class);
        return comment;
    }

    //保存评论点赞信息
    @Override
    public Integer saveCommentLike(Comment comment) {
        comment.setLikeCount(comment.getLikeCount()+1);
        mongoTemplate.save(comment);
        return comment.getLikeCount();
    }

    //取消评论点赞信息
    @Override
    public Integer removeCommentLike(Comment comment) {
        comment.setLikeCount(comment.getLikeCount()-1);
        mongoTemplate.save(comment);
        return comment.getLikeCount();
    }
}
