package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.*;
import com.itheima.service.mongo.UserCountsSrevice;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@DubboService
public class UserCountsSreviceImpl implements UserCountsSrevice {
    @Autowired
   private MongoTemplate mongoTemplate;


    //喜欢，粉丝，互相喜欢数量
    @Override
    public Map userLikeEachOther(Long userId) {
        //调用本类方法查询我喜欢的
        PageBeanVo pageBeanVo = iLike(userId, 1, 10);

        //获得我喜欢的好友个
        Long counts = pageBeanVo.getCounts();
        //调用本类中的方法查询喜欢我的
        PageBeanVo pageBeanVo1 = likeMe(userId, 1, 10);
        //获得粉丝的个数
        Long counts1 = pageBeanVo1.getCounts();
        //调用本类方法查询互相喜欢的
        PageBeanVo pageBeanVo2 = methodSwithEachOther(userId, 1, 10);
        Long counts2 = pageBeanVo2.getCounts();
        //封装
        Map<String, Object> map = new HashMap<>();
        map.put("eachLoveCount",counts2);//互相喜欢
        map.put("loveCount",counts);//我喜欢
        map.put("fanCount",counts1);//喜欢我

        return map;
    }

    //喜欢，粉丝，互相喜欢详情查询
    @Override
    public PageBeanVo likeDetails(Integer type, Long userId,Integer pageNum,Integer pageSize) {
        switch (type){
            case 1:{//互相喜欢
                List<Long> list= new ArrayList<>();
                //调用本类方法
                 PageBeanVo pageBeanVo = methodSwithEachOther(userId, pageNum, pageSize);
                    //获得好友id

                List<Friend> friendList = (List<Friend>) pageBeanVo.getItems();
                if (CollectionUtil.isNotEmpty(friendList)) {

                    for (Friend friend : friendList) {
                        list.add(friend.getFriendId());
                    }
                }
                pageBeanVo.setItems(list);

                //返回分页结果
                return pageBeanVo;




            }
            case 2:{//我喜欢
                //声明
                List<Long> list = new ArrayList<>();
                //调用本类方法
                PageBeanVo pageBeanVo = iLike(userId, pageNum, pageSize);
                List<UserLike> userLikeList = (List<UserLike>) pageBeanVo.getItems();
                if (CollectionUtil.isNotEmpty(userLikeList)) {
                    for (UserLike userLike : userLikeList) {
                        list.add(userLike.getLikeUserId());
                    }
                }
                    pageBeanVo.setItems(list);

                return pageBeanVo;



            }
            case 3:{//喜欢我
                //声明
                List<Long> list = new ArrayList<>();
                //调用本类方法
            PageBeanVo pageBeanVo = likeMe(userId, pageNum, pageSize);
                List<UserLike> userLikeList = (List<UserLike>) pageBeanVo.getItems();
                if (CollectionUtil.isNotEmpty(userLikeList)) {
                    for (UserLike userLike : userLikeList) {
                        list.add(userLike.getUserId());


                    }
                }
                    pageBeanVo.setItems(list);

                return  pageBeanVo;

            }
            case 4:{//谁看过我
                //构造条件
                Query query = Query.query(Criteria.where("userId").is(userId))
                        .with(Sort.by(Sort.Order.desc("date")))
                        .skip((pageNum - 1) * pageSize).limit(pageSize);
                //查询来客
                List<Visitor> visitors = mongoTemplate.find(query, Visitor.class);
                //声明

                long count = mongoTemplate.count(query, Visitor.class);

                return new PageBeanVo(pageNum,pageSize,count,visitors);

            }
        }
        return null;
    }
//    //喜欢
//    @Override
//    public Integer SomeLike(Long userId, Long xihuanId) {
//        //添加喜欢关系
//        UserLike userLike = new UserLike();
//        userLike.setUserId(userId);
//        userLike.setLikeUserId(xihuanId);
//        userLike.setCreated(System.currentTimeMillis());
//        mongoTemplate.save(userLike);
//        //查询对方是否喜欢你
//        Query query = Query.query(Criteria.where("userId").is(xihuanId).and("likeUserId").is(userId));
//        boolean exists = mongoTemplate.exists(query, UserLike.class);
//
//        // 1.2 判断
//        if (exists == true) { //他也喜欢你
//
//
//            Friend myJiaFriend = new Friend();
//            myJiaFriend.setCreated(System.currentTimeMillis());
//            myJiaFriend.setUserId(userId); // 我的id
//            myJiaFriend.setFriendId(xihuanId); // 好友id
//            mongoTemplate.save(myJiaFriend);
//            // 1.3 查询好友的个人动态表
//            List<MyMovement> friendMyMovementList = mongoTemplate.findAll(MyMovement.class, ConstantUtil.MOVEMENT_MINE + xihuanId);
//            // 1.4 遍历
//            if (CollectionUtil.isNotEmpty(friendMyMovementList)) {
//                for (MyMovement friendMyMovement : friendMyMovementList) {
//                    // 1.5 添加到我的好友动态表
//                    FriendMovement myFriendMovement = new FriendMovement();
//                    myFriendMovement.setUserId(xihuanId); // 好友id
//                    myFriendMovement.setCreated(friendMyMovement.getCreated()); // 好友动态发布时间
//                    myFriendMovement.setPublishId(friendMyMovement.getPublishId()); // 好友的动态id
//                    // 保存我的好友动态表
//                    mongoTemplate.save(myFriendMovement, ConstantUtil.MOVEMENT_FRIEND + userId);
//
//                }
//
//            }
//            Friend friendJiaMy = new Friend();
//            friendJiaMy.setCreated(System.currentTimeMillis());
//            friendJiaMy.setUserId(xihuanId); // 好友的id
//            friendJiaMy.setFriendId(userId); // 你的id
//            mongoTemplate.save(friendJiaMy);
//            // 2.3  查询我的个人动态表
//            List<MyMovement> myMovementList = mongoTemplate.findAll(MyMovement.class, ConstantUtil.MOVEMENT_MINE + userId);
//            // 2.4 遍历
//            if (CollectionUtil.isNotEmpty(myMovementList)) {
//                for (MyMovement myMovement : myMovementList) {
//                    // 2.5 添加到他的好友动态表
//                    FriendMovement friendMovement = new FriendMovement();
//                    friendMovement.setCreated(myMovement.getCreated()); // 你的动态发布时间
//                    friendMovement.setUserId(userId); // 你的id
//                    friendMovement.setPublishId(myMovement.getPublishId()); // 你的动态id
//                    // 保存他的好友动态
//                    mongoTemplate.save(friendMovement, ConstantUtil.MOVEMENT_FRIEND + xihuanId);
//                }
//            }
//            return 1;
//        }
//        return 0;
//    }
//    //取消喜欢
//    @Override
//    public Integer deleteLike(Long userId, Long xihuanId) {
//        //删除喜欢表
//        Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(xihuanId));
//
//        mongoTemplate.remove(query,UserLike.class);
//        //删除好友关系
//        query= Query.query(Criteria.where("userId").is(userId).and("friendId"));
//        mongoTemplate.remove(query,Friend.class);
//
//
//
//
//        return 0;
//    }



    //互相喜欢方法
    public PageBeanVo methodSwithEachOther(Long userId,Integer pageNum,Integer pageSize){
        Query query = Query.query(Criteria.where("userId").is(userId)).skip((pageNum-1)*pageSize);
        List<Friend> friendList = mongoTemplate.find(query, Friend.class);
        long count = mongoTemplate.count(query, Friend.class);
        return new PageBeanVo(pageNum,pageSize,count,friendList);
    }
    //我喜欢方法
    public PageBeanVo iLike(Long userId,Integer pageNum,Integer pageSize){

        //构造条件
        Query query = Query.query(Criteria.where("userId").is(userId)).skip((pageNum-1)*pageSize);
        //查询获得我喜欢的
        List<UserLike> userLikes = mongoTemplate.find(query, UserLike.class);
        long count = mongoTemplate.count(query, UserLike.class);
        return  new PageBeanVo(pageNum,pageSize,count,userLikes);
    }
    //喜欢我方法
    public PageBeanVo likeMe(Long userId,Integer pageNum,Integer pageSize){
        //构造条件
        Query query1 = Query.query(Criteria.where("likeUserId").is(userId)).skip((pageNum-1)*pageSize).limit(pageSize);
        //查询喜欢我的
        List<UserLike> userLikes1 = mongoTemplate.find(query1, UserLike.class);
        long count = mongoTemplate.count(query1, UserLike.class);
        return new PageBeanVo(pageNum,pageSize,count,userLikes1);

    }
}

