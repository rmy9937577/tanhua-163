package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;
import com.itheima.service.mongo.SlideCardsService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class SlideCardsServiceImpl implements SlideCardsService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 查询推荐用户
    @Override
    public List<RecommendUser> findRecommendUser(Long userId) {
        // 1. 创建条件
        Query query = Query.query(Criteria.where("toUserId").is(userId));
        // 2. 进行查询
        List<RecommendUser> recommendUserList = mongoTemplate.find(query, RecommendUser.class);
        return recommendUserList;
    }

    // 删除推荐用户
    @Override
    public void remove(Long toUserId, Long userId) {
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(userId).and("toUserId").is(toUserId)), RecommendUser.class);
    }

    // 喜欢 并判断对方是否喜欢自己
    @Override
    public Boolean saveLove(UserLike userLike) {
        // 1. 保存喜欢
        mongoTemplate.save(userLike);
        // 2. 查询是否对方喜欢自己
        boolean exists = mongoTemplate.exists(Query.query(Criteria.where("userId").is(userLike.getLikeUserId()).and("likeUserId").is(userLike.getUserId())), UserLike.class);
        return exists;
    }

    // 喜欢 并判断对方是否喜欢自己
    @Override
    public Boolean removeLove(Long userId, Long likeUserId) {
        // 1. 删除喜欢
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId)), UserLike.class);
        // 2. 查询对方是否喜欢自己
        boolean exists = mongoTemplate.exists(Query.query(Criteria.where("userId").is(likeUserId).and("likeUserId").is(userId)), UserLike.class);
        return exists;
    }

    // 是否喜欢他（她）
    @Override
    public Boolean findAlreadyLove(Long userId, Long likeUserId) {
        return mongoTemplate.exists(Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId)), UserLike.class);
    }
}
