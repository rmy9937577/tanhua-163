package com.itheima.service.mongo.impl;


import cn.hutool.core.util.RandomUtil;
import com.alibaba.nacos.common.utils.CollectionUtils;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Sound;
import com.itheima.service.mongo.SoundService;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.SoundVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.stream.Collectors;


@DubboService
public class SoundServiceImpl implements SoundService {

    @Autowired
    private MongoTemplate mongoTemplate;
    @DubboReference
    private UserInfoService userInfoService;


    //发送语音(添加语音)
    @Override
    public void saveSound(Sound sound) {

        mongoTemplate.save(sound);
    }


    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //接收语音(查询并删除语音)
    @Override
    public SoundVo findSound(Long userId) {


        List<Sound> soundListRes = mongoTemplate.findAll(Sound.class);
        if (CollectionUtils.isNotEmpty(soundListRes)) {
            List<Sound> soundList = soundListRes.stream().filter(s -> s.getUserId().longValue() != (userId.longValue())).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(soundList)) {

                int v = RandomUtil.randomInt(0, soundList.size());
                Sound sound = soundList.get(v);
                SoundVo soundVo = new SoundVo();
                soundVo.setSoundUrl(sound.getSoundUrl());
                soundVo.setId(sound.getUserId().intValue());
                UserInfo userInfo = userInfoService.findUserInfoById(sound.getUserId());
                soundVo.setAge(userInfo.getAge());
                soundVo.setNickname(userInfo.getNickname());
                soundVo.setAvatar(userInfo.getAvatar());
                soundVo.setGender(userInfo.getGender());
                //剩余次数

                //redis取出
                String s = stringRedisTemplate.opsForValue().get("remainingTimes_" + userId);

                if (s == null || "".equals(s)) {
                    stringRedisTemplate.opsForValue().set("remainingTimes_" + userId, "10");
                } else {
                    Integer l = Integer.parseInt(s);
                    if (l == 0) {
                        soundVo.setRemainingTimes(0);
                        return soundVo;
                    } else {
                        Integer l1 = l--;
                        soundVo.setRemainingTimes(l1);

                        String s1 = String.valueOf(l);
                        stringRedisTemplate.opsForValue().set("remainingTimes_" + userId, s1);
                        //删除用过的语音
                        mongoTemplate.remove(sound);
                        return soundVo;
                    }

                }

            }
        }

        return null;

    }

}
