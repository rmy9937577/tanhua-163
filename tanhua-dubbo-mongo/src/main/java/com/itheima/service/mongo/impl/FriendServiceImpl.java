package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.Friend;
import com.itheima.domain.mongo.FriendMovement;
import com.itheima.domain.mongo.MyMovement;
import com.itheima.service.mongo.FriendService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import com.mongodb.client.result.DeleteResult;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class FriendServiceImpl implements FriendService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void addContacts(Long userId, Long friendId) {
        // 1.你加他
        // 1.1 判断是否是好友
        boolean myExits = mongoTemplate.exists(Query.query(Criteria.where("userId").is(userId).and("friendId").is(friendId)), Friend.class);
        // 1.2 判断
        if (myExits == false) { // 你加他
            Friend myJiaFriend = new Friend();
            myJiaFriend.setCreated(System.currentTimeMillis());
            myJiaFriend.setUserId(userId); // 我的id
            myJiaFriend.setFriendId(friendId); // 好友id
            mongoTemplate.save(myJiaFriend);
            // 1.3 查询好友的个人动态表
            List<MyMovement> friendMyMovementList = mongoTemplate.findAll(MyMovement.class, ConstantUtil.MOVEMENT_MINE + friendId);
            // 1.4 遍历
            if (CollectionUtil.isNotEmpty(friendMyMovementList)) {
                for (MyMovement friendMyMovement : friendMyMovementList) {
                    // 1.5 添加到我的好友动态表
                    FriendMovement myFriendMovement = new FriendMovement();
                    myFriendMovement.setUserId(friendId); // 好友id
                    myFriendMovement.setCreated(friendMyMovement.getCreated()); // 好友动态发布时间
                    myFriendMovement.setPublishId(friendMyMovement.getPublishId()); // 好友的动态id
                    // 保存我的好友动态表
                    mongoTemplate.save(myFriendMovement, ConstantUtil.MOVEMENT_FRIEND + userId);

                }
            }
        }

        // 2.他加你
        // 2.1 判断是否为好友
        boolean friendExits = mongoTemplate.exists(Query.query(Criteria.where("userId").is(friendId).and("friendId").is(userId)), Friend.class);
        // 2.2 判断
        if (friendExits == false) { // 他加你
            Friend friendJiaMy = new Friend();
            friendJiaMy.setCreated(System.currentTimeMillis());
            friendJiaMy.setUserId(friendId); // 好友的id
            friendJiaMy.setFriendId(userId); // 你的id
            mongoTemplate.save(friendJiaMy);
            // 2.3  查询我的个人动态表
            List<MyMovement> myMovementList = mongoTemplate.findAll(MyMovement.class, ConstantUtil.MOVEMENT_MINE + userId);
            // 2.4 遍历
            if (CollectionUtil.isNotEmpty(myMovementList)) {
                for (MyMovement myMovement : myMovementList) {
                    // 2.5 添加到他的好友动态表
                    FriendMovement friendMovement = new FriendMovement();
                    friendMovement.setCreated(myMovement.getCreated()); // 你的动态发布时间
                    friendMovement.setUserId(userId); // 你的id
                    friendMovement.setPublishId(myMovement.getPublishId()); // 你的动态id
                    // 保存他的好友动态
                    mongoTemplate.save(friendMovement, ConstantUtil.MOVEMENT_FRIEND + friendId);
                }
            }
        }

    }

    // 查看联系人列表
    @Override
    public PageBeanVo findContactsByPage(Long userId, Integer pageNum, Integer pageSize) {
        // 1.构建条件
        Query query = Query.query(Criteria.where("userId").is(userId))
                .skip((pageNum - 1) * pageSize).limit(pageSize);
        // 2.查询list
        List<Friend> friendList = mongoTemplate.find(query, Friend.class);
        // 3.查询total
        long total = mongoTemplate.count(query, Friend.class);
        // 4.返回并分装分页对象
        return new PageBeanVo(pageNum, pageSize, total, friendList);
    }

    // 删除好友
    @Override
    public void removeContacts(Long userId, Long friendId) {
        // 删除互相好友
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(userId).and("friendId").is(friendId)), Friend.class);
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(friendId).and("friendId").is(userId)), Friend.class);
        // 删除好友后互相删除存储的彼此的动态信息
        DeleteResult userId1 = mongoTemplate.remove(Query.query(Criteria.where("userId").is(userId)), ConstantUtil.MOVEMENT_FRIEND + friendId);
        DeleteResult userId2 = mongoTemplate.remove(Query.query(Criteria.where("userId").is(friendId)), ConstantUtil.MOVEMENT_FRIEND + userId);
        System.out.println(userId1 + "" + userId2);
    }
}
