package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Visitor;
import com.itheima.service.mongo.VisitorService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.swing.*;
import java.util.List;

@DubboService
public class VisitorServiceImpl implements VisitorService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Visitor> findeAccessUser(Long userId, Long lastLoginTime) {
        // 1.构建条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("date").gt(lastLoginTime))
                .skip(0).limit(5);

        // 2.查询
        List<Visitor> visitorList = mongoTemplate.find(query, Visitor.class);

        // 3.返回
        return visitorList;
    }
}
