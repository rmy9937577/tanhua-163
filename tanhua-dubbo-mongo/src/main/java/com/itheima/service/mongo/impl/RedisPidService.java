package com.itheima.service.mongo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

// 大数据pid主键生成器
@Component
public class RedisPidService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    public Long getNextPid(String collectionName){
        // 创建redis的long的主键自增器对象
        RedisAtomicLong redisAtomicLong = new RedisAtomicLong("pid:" + collectionName, stringRedisTemplate.getConnectionFactory());
        // 完成自增并返回
        return redisAtomicLong.incrementAndGet();
    }
}
