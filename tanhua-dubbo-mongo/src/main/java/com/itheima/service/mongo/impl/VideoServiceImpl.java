package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import com.itheima.domain.mongo.RecommendVideo;
import com.itheima.domain.mongo.Video;
import com.itheima.service.mongo.VideoService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class VideoServiceImpl implements VideoService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public PageBeanVo findVideoByPage(Long userId, Integer pageNum, Integer pageSize) {
        // 1.构建查询条件
        Query query = Query.query(Criteria.where("userId").is(userId))
                .with(Sort.by(Sort.Order.desc("date")))
                .skip((pageNum - 1) * pageSize).limit(pageSize);
        // 2.查询推荐列表
        List<RecommendVideo> recommendVideoList = mongoTemplate.find(query, RecommendVideo.class);
        // 3.遍历并查询视频详情
        List<Video> videoList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(recommendVideoList)) {
            for (RecommendVideo recommendVideo : recommendVideoList) {
                // 查询视频详情
                Video video = mongoTemplate.findById(recommendVideo.getVideoId(), Video.class);
                // 添加到集合
                videoList.add(video);
            }
        }
        // 4.查询total
        long total = mongoTemplate.count(query, RecommendVideo.class);
        // 5.封装并返回分页对象
        return new PageBeanVo(pageNum, pageSize, total, videoList);
    }

    @Autowired
    private RedisPidService redisPidService;

    // 发布视频
    @Override
    public void saveVideo(Video video) {
        // 1.设置视频大数据vid
        video.setVid(redisPidService.getNextPid(ConstantUtil.VIDEO_ID));
        // 2.调用mongo保存
        mongoTemplate.save(video);

        // 3.推荐给自己
        RecommendVideo recommendVideo = new RecommendVideo();
        recommendVideo.setDate(System.currentTimeMillis()); // 推荐时间
        recommendVideo.setVid(video.getVid()); // 大数据id
        recommendVideo.setUserId(video.getUserId()); // 自己的id
        recommendVideo.setVideoId(video.getId()); // 视频id
        recommendVideo.setScore(RandomUtil.randomDouble(60, 99));
        mongoTemplate.save(recommendVideo);
    }
    //根据个人用户查找其所有视频
    @Override
    public PageBeanVo findVideosByUserId(Long userId, Integer pageNum, Integer pageSize) {
        //1.创建查找条件
        Query query = Query.query(Criteria.where("userId").is(userId)).skip((pageNum - 1) * pageSize);
        //2.查找集合
        List<Video> videoList = mongoTemplate.find(query, Video.class);
        //封装分页对象
        PageBeanVo paginationVo = new PageBeanVo();
        if (videoList.size() > 0){
            paginationVo.setItems(videoList);
            paginationVo.setPagesize(pageSize);
            paginationVo.setPage(pageNum);
            paginationVo.setCounts(mongoTemplate.count(query,Video.class));
            return paginationVo;
        }else {
            return null;
        }
    }


    //根据id查询视频
    @Override
    public Video findVideo(String id) {
        Video video = mongoTemplate.findById(id, Video.class);
        return video;
    }
}
