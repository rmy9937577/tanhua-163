package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.UserLocation;
import com.itheima.service.mongo.UserLocationService;
import com.itheima.vo.UserAndMapVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class UserLocationServiceImpl implements UserLocationService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveUserLocation(Double longitude, Double latitude, String addrStr, Long userId) {
        // 1.先查
        UserLocation userLocation = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), UserLocation.class);

        // 2.判断
        if (userLocation == null) { // 新增
            userLocation = new UserLocation();
            userLocation.setUserId(userId); // 用户id
            userLocation.setLocation(new GeoJsonPoint(longitude, latitude)); // 经纬度
            userLocation.setAddress(addrStr); // 地址描述
            userLocation.setCreated(System.currentTimeMillis()); // 创建时间
            userLocation.setUpdated(System.currentTimeMillis()); // 更新时间
            userLocation.setLastUpdated(System.currentTimeMillis()); // 上次更新时间
            // 保存
            mongoTemplate.save(userLocation);
        } else {// 更新
            userLocation.setLocation(new GeoJsonPoint(longitude, latitude)); // 经纬度
            userLocation.setAddress(addrStr); // 地址描述
            userLocation.setLastUpdated(userLocation.getUpdated()); // 上次更新时间
            userLocation.setUpdated(System.currentTimeMillis()); // 本次更新时间
            // 因为有id了所以是更新
            mongoTemplate.save(userLocation);
        }
    }

    // 搜附近
    @Override
    public List<Long> searchNearUser(Long userId, Long distance) {
        // 1.找圆心
        GeoJsonPoint location = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), UserLocation.class).getLocation();
        // 2.指定半径范围
        Distance dis = new Distance(distance/1000, Metrics.KILOMETERS);
        // 3.画圆
        Circle circle = new Circle(location, dis);
        // 4.条件搜索
        Query query = Query.query(Criteria.where("location").withinSphere(circle));
        List<UserLocation> userLocationList = mongoTemplate.find(query, UserLocation.class);
        // 5.返回符合条件的用户id
        List<Long> nearUserIdList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(userLocationList)) {
            for (UserLocation userLocation : userLocationList) {
                nearUserIdList.add(userLocation.getUserId());
            }
        }
        return nearUserIdList;
    }
    //查找用户位置
    @Override
    public UserAndMapVo findByUserId(long userId) {
        UserLocation userLocation = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), UserLocation.class);
       // UserLocation userLocation1 = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(2)), UserLocation.class);


        UserAndMapVo userAndMapVo=new UserAndMapVo();
        if (userLocation==null){
            userLocation=new UserLocation();
           userLocation.setLocation(new GeoJsonPoint(116.403694, 39.914937));
        }
        userAndMapVo.setLongitude(userLocation.getLocation().getX());
        userAndMapVo.setLatitude(userLocation.getLocation().getY());
        return userAndMapVo;
    }
}
