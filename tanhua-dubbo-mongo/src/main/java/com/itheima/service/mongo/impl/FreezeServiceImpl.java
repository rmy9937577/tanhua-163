package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Freeze;
import com.itheima.service.mongo.FreezeService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;


@DubboService
public class FreezeServiceImpl implements FreezeService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 保存冻结人的冻结信息 或更改为解冻信息
    @Override
    public String saveFreeze(Freeze freeze) {
        mongoTemplate.save(freeze);
        return "操作成功";
    }

    // 根据用户id查询用户的状态
    @Override
    public Freeze findByUserId(Integer userId) {
        // 创建条件
        Query query = Query.query(Criteria.where("userId").is(userId));
        // 查询
        Freeze freeze = mongoTemplate.findOne(query, Freeze.class);
        return freeze;
    }


    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 在1库redis中存储时间和状态信息
    @Override
    public void saveStatus(Freeze freeze) {
        switch (freeze.getFreezingTime()) {
            case 1: // 向redis中存储状态 冻结三天
                stringRedisTemplate.opsForValue().set("freeze_" + freeze.getUserId(), freeze.getFreezingRange() + "_3", Duration.ofDays(3));
                break;
            case 2: // 向redis中存储状态 冻结七天
                stringRedisTemplate.opsForValue().set("freeze_" + freeze.getUserId(), freeze.getFreezingRange() + "_7", Duration.ofDays(7));
                break;
            case 3: // 向redis中存储状态 永久冻结
                stringRedisTemplate.opsForValue().set("freeze_" + freeze.getUserId(), freeze.getFreezingRange() + "_0");
                break;
            default:
                break;
        }
    }

    // 解冻时删除1库中的存储状态信息（仅仅适用于永久）
    @Override
    public Map<String, String> deleteStatus(Integer freezeDay, Freeze freeze, String frozenRemarks) {
        // 3. 声明一个map集合做返回结果
        Map<String, String> map = new HashMap<>();
        // 5. 将状态改为解冻状态
        freeze.setUserStatus("2");
        // 6. 解冻原因封装
        freeze.setReasonsForThawing(frozenRemarks);
        // 7. 将对象进行保存
        String message = saveFreeze(freeze);
        // 8. 存入集合
        map.put("message", message);
        // 9. 删除redis
        stringRedisTemplate.delete("freeze_" + freeze.getUserId());
        return map;
    }

    // 查询是否含有状态
    @Override
    public Boolean hasKey(Long userId) {
        return stringRedisTemplate.hasKey("freeze_" + userId);
    }

    // 将冻结时间到期的用户状态改为2
    @Override
    public String redisToFreeze(Long userId) {
        // 1. 取出redis中存储的用户状态
        String status = stringRedisTemplate.opsForValue().get("freeze_" + userId);
        // 2. 对字符串进行判断
        if (status == null) { // 说明该用户不存在冻结状态
            // 3. 取出用户状态
            Freeze freeze = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), Freeze.class);
            // 4. 修改冻结状态
            freeze.setUserStatus("2");
            // 5. 保存
            saveFreeze(freeze);
            // 6. 返回状态码为2
            return "2";
        }
        return "1";
    }


}
