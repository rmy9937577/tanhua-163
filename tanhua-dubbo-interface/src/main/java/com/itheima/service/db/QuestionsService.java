package com.itheima.service.db;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.Questions;

import java.util.List;

public interface QuestionsService {
    //保存题目
    void saveQuestion(Questions questions);
    //通过id删除
    void deleteById(Long id);
    //分页查询题目
    Page<Questions> findQuestionByPage(Long surveyId, Integer pageNum, Integer pageSize);

    List<Questions> findQuestionBySurveyId(Long id);
    //通过id查找
    Questions findQuestionById(Long id);
    //更新问题
    void update(Questions questions);
}
