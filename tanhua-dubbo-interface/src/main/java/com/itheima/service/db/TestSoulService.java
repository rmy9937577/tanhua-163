package com.itheima.service.db;

import com.itheima.domain.db.*;

import java.util.List;

/**
 * @Author RMY
 * @Date 2021/11/11 21:51
 * @Version 1.0
 */
//测灵魂接口
public interface TestSoulService {

    //保存一套初级试题
    void saveSurvey(Survey survey);

    //重新测试(更新)一套试题
    void updateReported(Reported reported);

    //查询一套试题
    List<Survey> findSurveyBySurveyIdList(List<Long> surveyIdList);

    //根据问卷Id查出所有问题
    List<Questions> findQuestionsBySurveyId(Long surveyId);

    //根据问卷id的集合查询该问题的所有选项
    List<Options> findOptionsByQuestionsId(Long questionsId);

    //根据选项id找到对应分数
    Options findByOptionId(long parseLong);

    //将计算的结果保存reported表
    Long saveReported(Reported reported);

    //根据结果Id查找结果
    Reported findByReportedId(Long reportedId);

    //根据id查找问题
    Questions findQuestionsByQuestionsId(Long questionsId);

    //查找分数相似度较高的用户
    List<Long> findSimilarYou(Long surveyId, Integer result);

    //查询用户锁题表
    List<QuestionUserLock> findQuestionUserLockByUserId(Long userId);

    //根据用户和问卷id查询锁表
    QuestionUserLock findQuestionUserLockByUserIdAndSurveyId(Long userId, Long surveyId);

    //保存锁表
    void saveQuestionUserLock(QuestionUserLock questionUserLock);

    //更新锁表
    void updateQuestionUserLock(QuestionUserLock questionUserLock);

    //根据用户Id查询他的所有成绩记录
    Reported findReportedByUserId(Long userId);


}
