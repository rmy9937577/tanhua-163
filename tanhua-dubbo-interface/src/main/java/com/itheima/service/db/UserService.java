package com.itheima.service.db;

import com.itheima.domain.db.User;

public interface UserService {

    // 保存用户，返回主键
    Long save(User user);

    // 根据手机号查询用户
    User findByPhone(String phone);
    // 更新手机号
    void update(Long id,String phone);

    // 根据id查询用户信息
    User findById(Long userId);
}
