package com.itheima.service.db;

import com.itheima.vo.PageBeanVo;

/**
 * @Author RMY
 * @Date 2021/11/10 19:25
 * @Version 1.0
 */
public interface AnnouncementService {
    //查询所有公告
    PageBeanVo findAllAnnouncement(Integer pageNum, Integer pageSize);
}
