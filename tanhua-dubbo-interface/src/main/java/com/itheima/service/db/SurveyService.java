package com.itheima.service.db;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Survey;


public interface SurveyService {
    //分页查找问卷
    Page<Survey> findSurveyByPage(Integer pageNum, Integer pageSize);
    //添加问卷
    void saveSurvey(Survey survey);
    //通过id删除问卷
    void deleteById(Long id);
    //更新问卷
    void update(Survey survey);
    //通过id查找
    Survey findSurveyById(Long id);
}
