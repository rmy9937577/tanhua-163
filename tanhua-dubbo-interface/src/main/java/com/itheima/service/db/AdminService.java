package com.itheima.service.db;

import com.itheima.domain.db.Admin;

public interface AdminService {

    // 根据username查询对象
    Admin findByUsername(String username);
}
