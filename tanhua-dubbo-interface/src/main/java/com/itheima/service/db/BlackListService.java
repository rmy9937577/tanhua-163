package com.itheima.service.db;

import com.itheima.vo.PageBeanVo;

public interface BlackListService {

    // 分页查询黑名单
    PageBeanVo findByPage(Long userId, Integer pageNum, Integer pageSize);

    // 移除黑名单
    void deleteByCondition(Long userId,Long blackUserId);
}
