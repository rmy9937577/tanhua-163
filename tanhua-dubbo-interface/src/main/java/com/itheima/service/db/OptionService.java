package com.itheima.service.db;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Options;


public interface OptionService {
    //保存选项
    void saveOption(Options options);
    //分页查询
    Page<Options> findByPage(Long questionId, Integer pageNum, Integer pageSize);
    //删除
    void  deleteOption(Long id);
    //通过题目id删除选项
    void deleteOptionByQuestionId(Long id);
    //通过id查询
    Options findOptionsById(Long id);
    //更新选项
    void updateOptions(Options options);
}
