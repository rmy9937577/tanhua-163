package com.itheima.service.db;

import com.itheima.domain.db.Notification;

public interface NotificationService {

    // 根据用户id查询推送通知
    Notification findByUserId(Long userId);

    // 保存推送通知
    void save(Notification notification);

    // 更新推送通知
    void update(Notification notification);
}
