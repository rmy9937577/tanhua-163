package com.itheima.service.db;

import com.itheima.domain.db.AnalysisByDay;
import com.itheima.vo.AnalysisSummaryVo;
import com.itheima.vo.DashboardVo;
import com.itheima.vo.LineChartVo;

import java.util.List;

public interface AnalysisByDayService {

    // 处理日志保存每日分析表中
    void logToAnalysisByDay();

    // 首页统计展示
    AnalysisSummaryVo findAnalysisSummaryVo();

    // 主页展示新增、活跃用户、次日留存率折线图
    List<DashboardVo> findDashboardVo(Long startTime, Long endTime, String type);

}
