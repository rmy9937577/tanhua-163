package com.itheima.service.mongo;


import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;

import java.util.List;

public interface SlideCardsService {

    // 查询推荐用户
    List<RecommendUser> findRecommendUser(Long userId);

    // 删除推荐用户
    void remove(Long toUserId, Long userId);

    // 喜欢 并判断对方是否喜欢自己
    Boolean saveLove(UserLike userLike);

    // 不喜欢 并判断对方是否喜欢自己
    Boolean removeLove(Long userId, Long likeUserId);

    // 是否喜欢他（她）
    Boolean findAlreadyLove(Long userId, Long likeUserId);
}
