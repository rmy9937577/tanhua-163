package com.itheima.service.mongo;

import com.itheima.vo.PageBeanVo;

public interface FriendService {

    // 添加好友
    void addContacts(Long userId, Long friendId);

    // 查看联系人列表
    PageBeanVo findContactsByPage(Long userId, Integer pageNum, Integer pageSize);

    // 删除好友
    void removeContacts(Long userId, Long friendId);
}
