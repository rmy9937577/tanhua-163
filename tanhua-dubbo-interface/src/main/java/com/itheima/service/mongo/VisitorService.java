package com.itheima.service.mongo;

import com.itheima.domain.mongo.Visitor;

import java.util.List;

public interface VisitorService {


    // 查询最近5个访客
    List<Visitor> findeAccessUser(Long userId,Long lastLoginTime);
}
