package com.itheima.service.mongo;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.vo.PageBeanVo;

public interface RecommendUserService {

    // 查询今日佳人
    RecommendUser findMaxScoreRecommendUser(Long toUserId);

    // 查询推荐用户
    PageBeanVo findRecommendUserByPage(Long toUserId, Integer pageNum, Integer pageSize);

    /**
     *
     * @param toUserId   你的id
     * @param userId     佳人的id
     * @return
     */
    RecommendUser findRecommendUserByCondition(Long toUserId,Long userId);
}
