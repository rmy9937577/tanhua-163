package com.itheima.service.mongo;

import com.itheima.domain.mongo.UserLocation;
import com.itheima.vo.UserAndMapVo;

import java.util.List;

public interface UserLocationService {

    // 上传地理位置
    void saveUserLocation(Double longitude, Double latitude, String addrStr, Long userId);

    // 搜附近
    List<Long> searchNearUser(Long userId, Long distance);
    //通过userid查找
    UserAndMapVo findByUserId(long userId);
}
