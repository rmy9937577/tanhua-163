package com.itheima.service.mongo;

import com.itheima.domain.mongo.Video;
import com.itheima.vo.PageBeanVo;

public interface VideoService {

    // 分页查询推荐视频
    PageBeanVo findVideoByPage(Long userId, Integer pageNum, Integer pageSize);

    // 发布视频
    void saveVideo(Video video);

    //查找每个用户发布的所有视频
    PageBeanVo findVideosByUserId(Long userId,Integer pageNum,Integer pageSize);

    Video findVideo(String id);
}
