package com.itheima.service.mongo;


import com.itheima.vo.PageBeanVo;

import java.util.Map;

public interface UserCountsSrevice {

    //喜欢，粉丝，互相喜欢数量
    Map userLikeEachOther(Long useId);
    //喜欢，粉丝，互相喜欢详情查询
    PageBeanVo likeDetails(Integer type, Long userId, Integer pageNum, Integer pageSize);
//    //喜欢
//    Integer  SomeLike(Long userId,Long xihuanId);
//    //取消喜欢
//  Integer deleteLike(Long userId,Long xihuanId);
}
