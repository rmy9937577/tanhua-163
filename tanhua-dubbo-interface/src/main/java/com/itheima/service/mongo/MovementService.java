package com.itheima.service.mongo;

import com.itheima.domain.mongo.Movement;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;

public interface MovementService {

    // 发布动态
    void saveMovement(Movement movement);

    // 查看个人动态
    PageBeanVo findMyMovement(Long userId, Integer pageNum, Integer pageSize);

    // 查看好友动态
    PageBeanVo findFriendMovement(Long userId, Integer pageNum, Integer pageSize);

    // 查看推荐动态
    PageBeanVo findRecommendMovement(Long userId, Integer pageNum, Integer pageSize);

    // 查询动态详情
    Movement findById(ObjectId id);

    // 后台查询动态
    PageBeanVo findMovvementByPage(Integer state ,Long userId, Integer pageNum, Integer pageSize);

    void updateState(Movement movement);
}
