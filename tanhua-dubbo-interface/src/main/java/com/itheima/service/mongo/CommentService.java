package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;

public interface CommentService {

    // 保存comment
    Integer saveComment(Comment comment);

    // 删除comment
    Integer removeComment(ObjectId publishId, Long userId, Integer commentType);

    // 分页查询评论
    PageBeanVo findByPage(ObjectId publishId, Integer commentType, Integer pageNum, Integer pageSize);

    // 查看点赞、评论、喜欢的用户信息
    PageBeanVo findCommentTypeByPage(Long publishUserId, Integer commentType, Integer pageNum, Integer pageSize);

    //查询单条评论
    Comment findCommentById(String commentId);

    //保存评论点赞信息
    Integer saveCommentLike(Comment comment);

    //取消评论点赞信息
    Integer removeCommentLike(Comment comment);
}
