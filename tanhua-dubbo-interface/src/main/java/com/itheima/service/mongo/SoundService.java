package com.itheima.service.mongo;

import com.itheima.domain.mongo.Sound;
import com.itheima.vo.SoundVo;

public interface SoundService {


    //发送语音(添加语音)
    void saveSound(Sound sound);

    //接收语音(查询并删除语音)
    SoundVo findSound(Long userId);

}
