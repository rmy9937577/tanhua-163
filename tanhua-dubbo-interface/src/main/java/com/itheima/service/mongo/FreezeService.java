package com.itheima.service.mongo;

import com.itheima.domain.mongo.Freeze;

import java.util.Map;

public interface FreezeService {

    // 保存冻结人的冻结 信息 或更改为解冻信息
    String saveFreeze(Freeze freeze);

    // 查询冻结人信息
    Freeze findByUserId(Integer userId);

    // 在1库redis中存储时间和状态信息
    void saveStatus(Freeze freeze);

    // 解冻时删除1库中的存储状态信息（仅仅适用于永久）
    Map<String, String> deleteStatus(Integer freezeDay,Freeze freeze, String frozenRemarks);

    // 查询是否含有状态
    Boolean hasKey(Long userId);

    // 将冻结时间到期的用户状态改为2
    String redisToFreeze(Long userId);

}
