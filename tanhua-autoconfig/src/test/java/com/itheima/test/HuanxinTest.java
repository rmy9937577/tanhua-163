package com.itheima.test;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class HuanxinTest {

    @Test
    public void test01()throws Exception{
        // 1.指定url地址
        String url = "https://a1.easemob.com/1116201127148586/tanhua/token";
        // 2.创建restTemplate对象
        RestTemplate restTemplate = new RestTemplate();
        // 3.指定请求体参数
        Map param = new HashMap();
        param.put("grant_type", "client_credentials");
        param.put("client_id", "YXA6nr-B8mpcSLGfnjVUNSK1gw");
        param.put("client_secret", "YXA6c3CRBW6Hq7kKFGMtwlOwkbl2fvY");

        // 4.发送post请求
        Map resultMap = restTemplate.postForObject(url, param, Map.class);
        Object token = resultMap.get("access_token");
        System.out.println(token);
    }
}
